package com.example.hannaheunice.rsapp;

/**
 * Created by john doe on 2/3/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*
 Some of this code is developed from samples from the Google WiFi Direct API Guide
 http://developer.android.com/guide/topics/connectivity/wifip2p.html
 */

public class WiFiClientBroadcastReceiver extends BroadcastReceiver {

    //private WifiP2pManager manager;
    private WifiP2pManager mWifiManager;
    private Channel channel;
    private Sender activity;
    private List<WifiP2pDevice> peers=new ArrayList<WifiP2pDevice>();

    public WiFiClientBroadcastReceiver(WifiP2pManager manager, Channel channel, Sender activity) {
        super();
        this.mWifiManager = manager;
        this.channel = channel;
        this.activity = activity;
        Log.i("tost","Client Broadcast receiver created");

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);

            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                Log.i("tost","Wifi Direct is enabled");
            } else {
                Log.i("tost","Wifi Direct is not enabled");
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            //This broadcast is sent when status of in range peers changes. Attempt to get current list of peers.

            mWifiManager.requestPeers(channel, new WifiP2pManager.PeerListListener() {

                public void onPeersAvailable(WifiP2pDeviceList peers) {

                    activity.displayPeers(peers);

                }
            });

            //update UI with list of peers

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

            NetworkInfo networkState = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
            WifiP2pInfo wifiInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);
            WifiP2pDevice device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);

            if(networkState.isConnected())
            {
                //set client state so that all needed fields to make a transfer are ready

                //activity.setTransferStatus(true);
                activity.setNetworkToReadyState(true, wifiInfo, device);
                mWifiManager.requestConnectionInfo(channel,connectionInfoListener);
                Log.i("tost","Connection Status: Connected");
            }
            else
            {
                //set variables to disable file transfer and reset client back to original state

                activity.setTransferStatus(false);
                Log.i("tost","Connection Status: Disconnected");
                mWifiManager.cancelConnect(channel, null);

            }
            //activity.setClientStatus(networkState.isConnected());

            // Respond to new connection or disconnections
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
        }





    }


    WifiP2pManager.ConnectionInfoListener connectionInfoListener=new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo info) {
//            InetAddress groupOwnerAddress=info.groupOwnerAddress;
            if(info.groupFormed){
                if(info.isGroupOwner){
                    Toast.makeText(activity, "Group owner",
                            Toast.LENGTH_SHORT).show();
                    mWifiManager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                        @Override
                        public void onGroupInfoAvailable(WifiP2pGroup group) {
                            Log.i("tost",group.toString());
                            activity.setGroupInfo(group);
                        }
                    });
                }else{
                    Toast.makeText(activity, "client",
                            Toast.LENGTH_SHORT).show();
                        activity.sendFile();
                }
            }
        }
    };

    private WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {
            Log.i("tost", "DEATH 2 AMERICA");
            Collection<WifiP2pDevice> refreshedPeers =  peerList.getDeviceList();
            if (!refreshedPeers.equals(peers)) {
                peers.clear();
                peers.addAll(refreshedPeers);

                // If an AdapterView is backed by this data, notify it
                // of the change.  For instance, if you have a ListView of
                // available peers, trigger an update.
//                    (( WiFiPeerListAdapter ) getListAdapter()).notifyDataSetChanged();

                // Perform any other updates needed based on the new list of
                // peers connected to the Wi-Fi P2P network.
                activity.displayPeers(peerList);
            }

            if (peers.size() == 0) {
                Log.i("tost", "wifi peers No devices found");
                return;
            }
        }
    };
}