package com.example.hannaheunice.rsapp;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by john doe on 2/27/2017.
 */

public class ImageAdjust extends AppCompatActivity{
    private ImageView mImageView;
    private Mat Pat=new Mat();
    private Mat PatOrig;
    private Mat tempPat=new Mat();
//    private String filename = Environment.getExternalStorageDirectory().getPath() +"/Pic.jpg";
    private String filename ;

    private String mImageFileName;

    private Bitmap myBitmap;

    private boolean isLego=false;
    private File imgFile;

    private double contrastPerc=1.0;
    private double brightnsPerc=0;

    private  File croppedPhoto;
    private File mImageFolder;
    private File imgfolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_adjust);

        Intent in = this.getIntent();
        filename=(String) in.getExtras().get("file");
        imgfolder=(File) in.getExtras().get("folder");


        PatOrig = Imgcodecs.imread(filename);
        //PatOrig.copyTo(Pat);

        Imgproc.resize(PatOrig,Pat,new Size(PatOrig.cols()/2,PatOrig.rows()/2));
        Imgcodecs.imwrite(filename,Pat);
        Pat.copyTo(tempPat);
        Imgproc.cvtColor(tempPat,tempPat,Imgproc.COLOR_BGRA2RGBA);

        File imgFile = new  File(filename);

        mImageView = (ImageView) findViewById(R.id.previ);
        myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        if(imgFile.exists()){
            refreshImage();
        }

        createImageFolder();

        Button done =(Button) findViewById(R.id.adjustBtn);

        SeekBar niggers = (SeekBar) findViewById(R.id.contrast);

        SeekBar figgers= (SeekBar) findViewById(R.id.brightness);

        niggers.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
             //   if(isLego) {
                    if (i > 50) {
                        contrastPerc = i / 50.00;
                    } else {
                        contrastPerc = 0.25 + (i*0.015);
                    }
                    Pat.convertTo(tempPat,  -1, contrastPerc, brightnsPerc);
                    Imgproc.cvtColor(tempPat,tempPat,Imgproc.COLOR_BGRA2RGBA);
                    //Imgcodecs.imwrite(filename, tempPat);
                    refreshImage();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        figgers.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                brightnsPerc=(i*2)-100;
                Pat.convertTo(tempPat,  -1, contrastPerc, brightnsPerc);
                Imgproc.cvtColor(tempPat,tempPat,Imgproc.COLOR_BGRA2RGBA);
                //Imgcodecs.imwrite(filename, tempPat);
                refreshImage();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatOrig.copyTo(tempPat);
                PatOrig.convertTo(tempPat,  -1, contrastPerc, brightnsPerc);
                //Imgproc.cvtColor(tempPat,tempPat,Imgproc.COLOR_BGRA2RGBA);

                Imgcodecs.imwrite(filename,tempPat);


                try {
                    Uri picUri= Uri.fromFile(new File(filename));
                    Log.d("crop","uri="+picUri);
                    Intent cropIntent = new Intent("com.android.camera.action.CROP");
                    // indicate image type and Uri
                    cropIntent.setDataAndType(picUri, "image/*");
                    // set crop properties here
                    cropIntent.putExtra("crop", true);
                    cropIntent.putExtra("scale", true);
                    cropIntent.putExtra(MediaStore.EXTRA_OUTPUT,getCropImageUri());
                    cropIntent.putExtra("outputFormat",Bitmap.CompressFormat.PNG.toString());
                    // start the activity - we handle returning in onActivityResult
                    startActivityForResult(cropIntent, 2);
                }
                // respond to users whose devices do not support the crop action
                catch (ActivityNotFoundException anfe) {
                    // display an error message
                    String errorMessage = "Whoops - your device doesn't support the crop action!";
                }
            }
        });
    }

    private void refreshImage(){
        Utils.matToBitmap(tempPat,myBitmap);
        mImageView.setImageBitmap(myBitmap);
    }

    private void createImageFolder(){
        File imageFolder= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        mImageFolder=new File(imageFolder,"RushScan");
        if(!mImageFolder.exists()){
            mImageFolder.mkdirs();
        }
    }

    private File createImageFileName(String type) throws IOException {
        String timestamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend="IMG_"+timestamp+"_"+type;
        File imageFile=File.createTempFile(prepend,".png",mImageFolder);
        mImageFileName=imageFile.getAbsolutePath();
        return imageFile;
    }

    public Uri getCropImageUri(){
        //Save cropped image
        try{
            croppedPhoto=createImageFileName("Crop");

        }catch(IOException e){
            e.printStackTrace();
        }
        //end
        Uri picUri= Uri.fromFile(new File(croppedPhoto.getAbsolutePath()));
        return picUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (data != null) {
                String cropFile=croppedPhoto.getAbsolutePath();
                Log.d("CROP","CROPPED SAVE");
                OpencvClass.scanCrop(cropFile);
                Log.d("CROP","THRESHOLDED");

                this.stopService(this.getIntent());

                Intent i = new Intent(ImageAdjust.this, Preview.class);
                i.putExtra("origfile",filename);
                i.putExtra("file",cropFile);
                i.putExtra("folder",mImageFolder);
                startActivity(i);
            }
        }
    }
}
