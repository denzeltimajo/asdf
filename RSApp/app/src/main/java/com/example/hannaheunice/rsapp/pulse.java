package com.example.hannaheunice.rsapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Environment;
import android.os.ResultReceiver;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

//import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import com.example.hannaheunice.rsapp.PulsatorLayout;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class pulse extends AppCompatActivity {
    private PulsatorLayout mPulsator;
    private TextView mCountText;
    private TextView mDurationText;

    private String fName=null;

    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse);


        mPulsator = (PulsatorLayout) findViewById(R.id.pulsator);
        //mCountText = (TextView) findViewById(R.id.text_count);
        // mDurationText = (TextView) findViewById(R.id.text_duration);
        Button stop_btn = (Button) findViewById(R.id.stopbut);

        // count seek bar
       /* SeekBar countSeek = (SeekBar) findViewById(R.id.seek_count);
        countSeek.setOnSeekBarChangeListener(mCountChangeListener);
        countSeek.setProgress(mPulsator.getCount() - 1);

        /* duration seek bar
        SeekBar durationSeek = (SeekBar) findViewById(R.id.seek_duration);
        durationSeek.setOnSeekBarChangeListener(mDurationChangeListener);
        durationSeek.setProgress(mPulsator.getDuration() / 100);

        // start pulsator
        mPulsator.start();

        stop_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPulsator.stop();
            }
        });
    }
   */

        public AlertDialog.Builder mBuilder;
        public static AlertDialog dialog;

        public ListView lv;
        public List<RowItem> rowItems;
        public static final String[] titles = {"Augment", "Save"};

        public static final Integer[] images = {R.drawable.ar_64, R.drawable.pdf_64};


        //SERVER VARS
        public final int fileRequestID = 55;
        public final int port = 7950;


        private WifiP2pManager wifiManag;
        private WifiP2pManager.Channel wifichannel;
        private BroadcastReceiver wifiServerReceiver;

        private IntentFilter wifiServerReceiverIntentFilter;

        private String path;
        private File downloadTarget;

        private Intent serverServiceIntent;

        private boolean serverThreadActive;

        //PEER RECIEVER VARS
        private WifiP2pManager mP2PManager;
        private WifiP2pManager.Channel mChannel;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_pulse);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            });


            rowItems = new ArrayList<RowItem>();
            for (int i = 0; i < titles.length; i++) {
                RowItem item = new RowItem(images[i], titles[i], " ");
                rowItems.add(item);
            }


            lv = new ListView(this);
            CustomBaseAdapter adapter = new CustomBaseAdapter(this, rowItems);
            lv.setAdapter(adapter);


            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch(i){
                        case 0:
                            //augment action
                            Toast.makeText(pulse.this, "Augment", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            startAugment();

                            break;
                        case 1:
                            //save
                            //Toast.makeText(pulse.this, "Save as PDF", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            saveAsPDF();

                            break;
                    }
                }
            });

            mBuilder = new AlertDialog.Builder(this);
            mBuilder.setView(lv);

            dialog = mBuilder.create();

            mPulsator = (PulsatorLayout) findViewById(R.id.pulsator);

            wifiManag = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
            wifichannel = wifiManag.initialize(this, getMainLooper(), null);
            wifiServerReceiver = new WifiBroadcastReceiver(wifiManag, wifichannel, this);

            wifiServerReceiverIntentFilter = new IntentFilter();

            wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
            wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
            wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
            wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);


            android.net.wifi.WifiManager wifi=(android.net.wifi.WifiManager) getSystemService(Context.WIFI_SERVICE);
            while(!wifi.isWifiEnabled()){
                Log.i("tost","wifi peers wifi enabled");
                wifi.setWifiEnabled(true);
            }
            //setServerFileTransferStatus("No File being transfered");
            //    ApManager.isApOn(Receiver.this); // check Ap state :boolean
            //    ApManager.configApState(Receiver.this); // change Ap state :boolean


            registerReceiver(wifiServerReceiver, wifiServerReceiverIntentFilter);






            path = Environment.getExternalStorageDirectory().getPath() + "/Pictures/RushScan";
            downloadTarget = new File(path);

            serverServiceIntent = null;
            serverThreadActive = false;

            mP2PManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
            mChannel=mP2PManager.initialize(this,getMainLooper(),null);

            mP2PManager.discoverPeers(this.mChannel,new WifiP2pManager.ActionListener(){

                @Override
                public void onSuccess() {

                    Log.i("tost","wifi peers discover success");
                }

                @Override
                public void onFailure(int reason) {
                    Log.i("tost","wifi peers discover failed");

                }
            });

            FloatingActionButton faggio=(FloatingActionButton)findViewById(R.id.fab);
            faggio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.show();
                }
            });

            startServer();



        //    Button stop_btn = (Button) findViewById(R.id.stopbut);

            mPulsator.start();


        }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            backPressed();
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }


    public void backPressed() {
        stopReceiver();
        disconnect();
    }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (resultCode == Activity.RESULT_OK && requestCode == fileRequestID) {
                //Fetch result
                File targetDir = (File) data.getExtras().get("file");

                if (targetDir.isDirectory()) {
                    if (targetDir.canWrite()) {
                        downloadTarget = targetDir;
                        Log.i("tost","Download directory set to " + targetDir.getName());

                    } else {
                        Log.i("tost","You do not have permission to write to " + targetDir.getName());
                    }

                } else {
                    Log.i("tost","The selected file is not a directory. Please select a valid download directory.");
                }

            }
        }

        public void startServer() {

            //If server is already listening on port or transfering data, do not attempt to start server service
            if (!serverThreadActive) {
                //Create new thread, open socket, wait for connection, and transfer file

                serverServiceIntent = new Intent(this, ServerService.class);
                serverServiceIntent.putExtra("saveLocation", downloadTarget);
                serverServiceIntent.putExtra("port", new Integer(port));
                serverServiceIntent.putExtra("sR", new ResultReceiver(null) {
                    @Override
                    protected void onReceiveResult(int resultCode, final Bundle resultData) {

                        if (resultCode == port) {
                            if (resultData == null) {
                                //Server service has shut down. Download may or may not have completed properly.
                                showToaster("File not recieved");
                                serverThreadActive = false;




                            } else {
                                //dialog.show();
//                                fName=ServerService.origFilename;
                                showToaster("File Recived");
                            }
                        }else{
                            showToaster("File not recieved");
                        }
                        fName=resultData.getString("message");
                        showToaster("F"+fName);
                    }
                });

                serverThreadActive = true;
                startService(serverServiceIntent);


                //Set status to running

            } else {
                //Set status to already running

                Log.i("tost","The server is already running");

            }
        }
        //create event na nattriger after
        //dialog.show();
        public void showToaster(String message){
            Toast.makeText(this,message,
                    Toast.LENGTH_SHORT).show();
        }


        protected void onDestroy() {
            super.onDestroy();
            Log.d("WIFI connect","wifi peers ON DESTROY");
        //unregisterReceiver(mReceiver);
            stopReceiver();
            disconnect();
            Log.d("WIFI connect","wifi peers END ON DESTROY");
        }

        private void stopReceiver()
        {
            try
            {
                unregisterReceiver(wifiServerReceiver);
            }
            catch(IllegalArgumentException e)
            {
                //This will happen if the server was never running and the stop button was pressed.
                //Do nothing in this case.
            }
        }

    public void fileRecieved(String message){
        Toast.makeText(this, "Transfer Complete",
                Toast.LENGTH_SHORT).show();
    }

    public void disconnect() {
        Log.i("tost", "wifi peers removeGroup ");
        if (mP2PManager != null && mChannel != null) {
            //Log.d("tost", "wifi peers removeGroup ");
            mP2PManager.requestGroupInfo(mChannel, new WifiP2pManager.GroupInfoListener() {
                @Override
                public void onGroupInfoAvailable(final WifiP2pGroup group) {
                    if (group != null && mP2PManager != null && mChannel != null
                            && group.isGroupOwner()) {
                        Log.d("tost", "wifi peers removeGroup info and group availlable");
                        mP2PManager.removeGroup(mChannel, new WifiP2pManager.ActionListener() { //disconnect from client

                            @Override
                            public void onSuccess() {

                                Log.i("tost", "wifi peers removeGroup onSuccess -"+group.getNetworkName());
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.i("tost", "wifi peers removeGroup onFailure -" + reason);
                            }
                        });
                    }
                }
            });
        }
        android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager)getSystemService(Context.WIFI_SERVICE);
        wifiManager.disconnect();
        wifiManager.setWifiEnabled(false);
    }

        public void startAugment(){
            Intent AR=new Intent(this,Augment_OpenGL.class);
            if(fName==null){
                Toast.makeText(this,"No Image Received",Toast.LENGTH_SHORT).show();
            }else{
                unregisterReceiver(wifiServerReceiver);
                AR.putExtra("filename",fName);
                startActivity(AR);
            }
        }

    public int saveAsPDF(){
        String filefolder=Environment.getExternalStorageDirectory().getPath() + "/Pictures/RushScan/";
        String[] flName=fName.split(Pattern.quote("."));

        Log.d("save","fname="+flName[0]);
        if(!new File(filefolder,flName[0]+".pdf").exists() ){
//            new File().renameTo(new File(filefolder,filenamevar+".png"));
//            filename=filefolder+"/"+filenamevar+".png";
            String pdfFilename=flName[0]+".pdf";
            //SAVE PDF
            Document newDocu=new Document();
            try {
                File pdfFile = new File(filefolder, pdfFilename);
//                mPdfFile=pdfFile;
                FileOutputStream fo = new FileOutputStream(pdfFile);
                PdfWriter.getInstance(newDocu, fo);
                Log.d("save as pdf", "save as pdf filename: " + pdfFile.getAbsoluteFile());

                newDocu.open();
                Image scannedImage=Image.getInstance(filefolder+"/"+fName);
                scannedImage.setAbsolutePosition(0, 0);
                scannedImage.setBorderWidth(0);
                scannedImage.scaleAbsolute(PageSize.A4);
                newDocu.add(scannedImage);
                newDocu.close();
                fo.close();

                Toast.makeText(this, "File saved", Toast.LENGTH_SHORT).show();
//                filenameet.setText("");
                return 0;
            } catch (DocumentException e) {
                Log.d("save as pdf","save as pdf file ex");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d("save as pdf","save as pdf file IO "+e.toString());
                e.printStackTrace();
            }
        }
        Toast.makeText(this, "File name already exist", Toast.LENGTH_SHORT).show();
        return 1;
    }



    private final SeekBar.OnSeekBarChangeListener mCountChangeListener
            = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mPulsator.setCount(progress + 1);
            mCountText.setText(String.format(Locale.US, "%d", progress + 1));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

    };

    private final SeekBar.OnSeekBarChangeListener mDurationChangeListener
            = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mPulsator.setDuration(progress * 100);
            mDurationText.setText(String.format(
                    Locale.US, "%.1f", progress * 0.1f));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

    };
}
