package com.example.hannaheunice.rsapp;

/**
 * Created by john doe on 2/13/2017.
 */
import android.content.*;
import android.net.wifi.*;
import android.text.format.Formatter;
import android.util.Log;

import java.lang.reflect.*;
import java.math.BigInteger;
import java.security.SecureRandom;

public class ApManager {

    private String ssid;

    //check whether wifi hotspot on or off
    public static boolean isApOn(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        try {
            Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifimanager);
        }
        catch (Throwable ignored) {}
        return false;
    }

    // toggle wifi hotspot on or off
    public static boolean configApState(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        WifiConfiguration wificonfiguration = new WifiConfiguration();
        try {
            // if WiFi is on, turn it off
            if(isApOn(context)) {
                wifimanager.setWifiEnabled(false);
            }


          //  WifiConfiguration netConfig = new WifiConfiguration();

            wificonfiguration.SSID = "RSAPP-"+createSSID();
            wificonfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            wificonfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wificonfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            wificonfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);


            Method method = wifimanager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifimanager, wificonfiguration, !isApOn(context));

            //change settings
            //boolean apstatus=(Boolean) method.invoke(wifimanager, netConfig,true);

            Method isWifiApEnabledmethod = wifimanager.getClass().getMethod("isWifiApEnabled");
            while(!(Boolean)isWifiApEnabledmethod.invoke(wifimanager)){};
            Method getWifiApStateMethod = wifimanager.getClass().getMethod("getWifiApState");
            int apstate=(Integer)getWifiApStateMethod.invoke(wifimanager);
            Method getWifiApConfigurationMethod = wifimanager.getClass().getMethod("getWifiApConfiguration");
            wificonfiguration=(WifiConfiguration)getWifiApConfigurationMethod.invoke(wifimanager);
            Log.e("CLIENT_AP", "\nSSID:"+wificonfiguration.SSID+"\nPassword:"+wificonfiguration.preSharedKey+"\n");
            Log.e("CLIENT_AP", Formatter.formatIpAddress(wifimanager.getDhcpInfo().serverAddress));
            //Log.e("CLIENT_AP", createSSID());


            return true;
        }
        catch (Exception e) {
            Log.e("CLIENT_AP",e.toString());
        }
        return false;
    }

    private static String createSSID(){
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32).substring(0,8);
    }
} // end of class
