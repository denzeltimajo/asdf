package com.example.hannaheunice.rsapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;

import org.opencv.android.JavaCameraView;

import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;

public class ScanCameraView extends JavaCameraView implements PictureCallback {

    private static final String TAG = "Sample::Tutorial3View";
    private String mPictureFileName;



    public ScanCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedColorEffects();
    }

    public boolean isEffectSupported() {
        return (mCamera.getParameters().getColorEffect() != null);
    }

    public String getEffect() {
        return mCamera.getParameters().getColorEffect();
    }

    public void setEffect(String effect) {
        Camera.Parameters params = mCamera.getParameters();
        params.setColorEffect(effect);
        mCamera.setParameters(params);
    }

    public List<Size> getCameraResolutionList() {
        return mCamera.getParameters().getSupportedPictureSizes();
    }

    public List<Size> getPreviewResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public void setPreviewResolution(Size resolution) {

        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;



        connectCamera(resolution.width,resolution.height);

        Log.i(TAG,"picc: "+ mCamera.getParameters().getPictureSize().width + " x "+ mCamera.getParameters().getPictureSize().height);
    }

    public void setCameraResolution(Size resolution) {
        Camera.Parameters params = mCamera.getParameters();


        Log.i(TAG,"picc: "+ params.getPictureSize().width + " x "+ params.getPictureSize().height);

        params.setJpegQuality(100);
        params.setPictureSize(resolution.width,resolution.height);


        mCamera.setParameters(params);
        Log.i(TAG,"picc: "+ mCamera.getParameters().getPictureSize().width + " x "+ mCamera.getParameters().getPictureSize().height);
    }

    public Size getResolution() {
        return mCamera.getParameters().getPreviewSize();
    }

    public void takePicture(final String fileName) {
        Log.i(TAG, "Taking picture");

        this.mPictureFileName = fileName;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);



        Camera.Parameters param;
        param = mCamera.getParameters();

        Camera.Size bestSize = null;
        List<Camera.Size> sizeList = mCamera.getParameters().getSupportedPictureSizes();
        bestSize = sizeList.get(0);
        for(int i = 1; i < sizeList.size(); i++){
            if((sizeList.get(i).width * sizeList.get(i).height) > (bestSize.width * bestSize.height)){
                bestSize = sizeList.get(i);
            }
        }

        /*
        List<Integer> supportedPreviewFormats = param.getSupportedPictureFormats();
        Iterator<Integer> supportedPreviewFormatsIterator = supportedPreviewFormats.iterator();
        while(supportedPreviewFormatsIterator.hasNext()){
            Integer previewFormat =supportedPreviewFormatsIterator.next();
            if (previewFormat == ImageFormat.YV12) {
                param.setPreviewFormat(previewFormat);
            }
        }
*/
        param.setJpegQuality(100);
        param.setPictureSize(bestSize.width, bestSize.height);


        mCamera.setParameters(param);
     //   mCamera.setDisplayOrientation(90);


        // PictureCallback is implemented by the current class
     //   mCamera.takePicture(null, null, this);
        mCamera.takePicture(null, null, this);

        //finish();
      /*  Thread.sleep(WAIT_GENERIC);
        mCamera.stopPreview();
        mCamera.release();
        */
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        // Write the image in a file (in jpeg format)
        try {
            FileOutputStream fos = new FileOutputStream(mPictureFileName);
            Log.i("tost", "Saving pic");
            fos.write(data);
            fos.close();
        } catch (java.io.IOException e) {
            Log.e("PictureDemo", "Exception in photoCallback", e);
        }

    }


}
