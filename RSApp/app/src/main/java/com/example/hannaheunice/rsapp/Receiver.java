package com.example.hannaheunice.rsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Receiver extends AppCompatActivity {

    public AlertDialog.Builder mBuilder;
    public static AlertDialog dialog;

    public ListView lv;
    public List<RowItem> rowItems;
    public static final String[] titles = {"Augment", "Save"};

    public static final Integer[] images = {R.drawable.ar_64, R.drawable.pdf_64};

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        //return super.onCreateOptionsMenu(menu);
//        getMenuInflater().inflate(R.menu.toggle, menu);
//        MenuItem item = menu.findItem(R.id.myswitch);
//        item.setActionView(R.layout.on_off_switch);
//        return true;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//
//            }
//        });

        rowItems = new ArrayList<RowItem>();
        for (int i = 0; i < titles.length; i++) {
            RowItem item = new RowItem(images[i], titles[i], " ");
            rowItems.add(item);
        }


        lv = new ListView(this);
        CustomBaseAdapter adapter = new CustomBaseAdapter(this, rowItems);
        lv.setAdapter(adapter);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        //augment action
                        Toast.makeText(Receiver.this, "Augment", Toast.LENGTH_SHORT).show();

                        break;
                    case 1:
                        //import photo event
                        Toast.makeText(Receiver.this, "Save as PDF", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        mBuilder = new AlertDialog.Builder(this);
        mBuilder.setView(lv);

        dialog = mBuilder.create();

        Intent intt = new Intent(Receiver.this, pulse.class);
        startActivity(intt);
    }


    //create event na nattriger after
    //dialog.show();


}