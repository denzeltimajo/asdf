package com.example.hannaheunice.rsapp;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static android.opengl.GLES20.glCullFace;
import static android.opengl.GLES20.glFrontFace;

/**
 * Created by carizanatividad on 1/20/17.
 */

public class Line {
    private final int mProgram;
    private FloatBuffer vertexBuffer;
    private ShortBuffer drawListBuffer;


    private final String vertexshadercode=
            "uniform mat4 uMVPmatrix; "+
                    "attribute vec4 vPosition;" +
                    "void main(){"+
                    " gl_Position= uMVPmatrix*vPosition;"+
                    "}";
    private final String fragmentshadercode=
            "precision mediump float;"
                    +"uniform vec4 fcolor;"
                    +" void main(){"
                    +" gl_FragColor=fcolor;"
                    +"}";

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float lineCoords[] = {
            0.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f
    };

    private short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices
    float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };
    float[] mCurrentModelMatrix = new float[16];
    private float[][] colors = {  // Colors of the 6 faces
            {1.0f, 0.5f, 0.0f, 1.0f},  // 0. orange
            {1.0f, 0.0f, 1.0f, 1.0f},  // 1. violet
            {0.0f, 1.0f, 0.0f, 1.0f},  // 2. green
            {0.0f, 0.0f, 1.0f, 1.0f},  // 3. blue
            {1.0f, 0.0f, 0.0f, 1.0f},  // 4. red
            {1.0f, 1.0f, 0.0f, 1.0f}   // 5. yellow
    };


    private int vpositionHandle;
    private int fcolorHandle;
    private int mvpHandle;
    private int vertex_cnt=lineCoords.length/COORDS_PER_VERTEX;
    private int vertex_stride=COORDS_PER_VERTEX*4;

    public Line() {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                lineCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(lineCoords);
        vertexBuffer.position(0);

        int vertexShader=MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,vertexshadercode);
        int fragmentShader=MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,fragmentshadercode);

        mProgram= GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram,vertexShader);
        GLES20.glAttachShader(mProgram,fragmentShader);
        GLES20.glLinkProgram(mProgram);
    }

    public void drawLine(float[] mvpMatrix){
        GLES20.glUseProgram(mProgram);
        glFrontFace(GLES20.GL_CCW);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        glCullFace(GLES20.GL_BACK);

        mCurrentModelMatrix=mvpMatrix.clone();
        float[] RTmatrix=new float[16];
        float[] topMatrix=new float[16];
        topMatrix=mvpMatrix.clone();
        RTmatrix=mvpMatrix.clone();
        float[][] rotatecube={{270.0f,0.0f,1.0f,0.0f},{180.0f,0.0f,1.0f,0.0f},{90.0f,0.0f,1.0f,0.0f},{270.0f,1.0f,0.0f,0.0f},{90.0f,1.0f,0.0f,0.0f}};
        for(int face=0;face<4;face++){
            vpositionHandle= GLES20.glGetAttribLocation(mProgram,"vPosition");
            GLES20.glEnableVertexAttribArray(vpositionHandle);
            GLES20.glVertexAttribPointer(vpositionHandle,COORDS_PER_VERTEX, GLES20.GL_FLOAT,false,vertex_stride,vertexBuffer);
            fcolorHandle= GLES20.glGetUniformLocation(mProgram,"fcolor");
            GLES20.glUniform4fv(fcolorHandle,1,colors[face],0);
            if(face>0){
                RTmatrix=rotate(RTmatrix,rotatecube[face-1][0],rotatecube[face-1][1],rotatecube[face-1][2],rotatecube[face-1][3]);
            }
            mCurrentModelMatrix=translate(RTmatrix,0.0f,0.0f,1.0f);

            mvpHandle= GLES20.glGetUniformLocation(mProgram,"uMVPmatrix");
            GLES20.glUniformMatrix4fv(mvpHandle,1,false,mCurrentModelMatrix,0);
            GLES20.glDrawArrays(GLES20.GL_LINE_STRIP,0,vertex_cnt);
            RTmatrix=topMatrix.clone();
            GLES20.glDisableVertexAttribArray(vpositionHandle);
        }
        GLES20.glDisable(GLES20.GL_CULL_FACE);
    }
    private int cnt=0;
    public float[] translate(float[] currentmatrix,float x, float y, float z)
    {

        float[] tempModelMatrix = new float[16];
        Matrix.setIdentityM(tempModelMatrix, 0);
        Matrix.translateM(tempModelMatrix,0,x,y,z);
        for(float mtrx:tempModelMatrix){
            Log.d("Matrix value no."+cnt,""+mtrx);
        }
        Matrix.multiplyMM(currentmatrix, 0,
                currentmatrix, 0,tempModelMatrix, 0);
        return currentmatrix;
    }
    public float[] rotate(float[] currentmatrix,float angle, float x, float y, float z)
    {
        float[] tempModelMatrix = new float[16];
        Matrix.setIdentityM(tempModelMatrix, 0);
        Matrix.rotateM(tempModelMatrix,0,angle,x,y,z);
        for(float mtrx:tempModelMatrix){
            Log.d("ROTATION: Matrix value no."+cnt,""+mtrx);
        }
        Log.d("ROTATION: Matrix value no."+cnt,"------");
        Log.w("face"+cnt,""+x+","+y+","+z);
        Matrix.multiplyMM(currentmatrix, 0,
                currentmatrix, 0,tempModelMatrix, 0);
        cnt++;
        if(cnt==6){
            cnt=0;
        }
        return currentmatrix;
    }


}
