package com.example.hannaheunice.rsapp;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by carizanatividad on 1/6/17.
 */

public class MyGLRenderer implements GLSurfaceView.Renderer {

    private  float[] posemodelMatrix=new float[16];
    private final float[] projectionMatrix=new float[16];
    private  float[] viewMatrix=new float[16];
    private final float[] mvpMatrix=new float[16];
    private final float[] mRotationMatrix=new float[16];

    private Cube2 sampleCube;
    private Line sampleLine;
    private static float angleCube = 0;
    private static float speedCube = -1.5f;
    public volatile float mAngle;
    Context context;

    public static String texturefile;


    public MyGLRenderer(Context context) {
//        this.context = context;
        sampleCube=new Cube2();
//        sampleLine=new Line();
    }


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
//        GLES20.glClearColor(0.0f,0.0f,0.0f,1.0f);
        sampleCube=new Cube2();
//        sampleCube.loadTexture();
        GLES20.glEnable(GLES20.GL_TEXTURE_2D);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0,0,width,height);
        GLES20.glDepthRangef(1,2);
//        gl.glMatrixMode(GL10.GL_PROJECTION);

        float ratio=(float) width/height;
        Matrix.frustumM(projectionMatrix,0,-ratio,ratio,-1,1,1,100);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

//        Matrix.setLookAtM(viewMatrix,0,0,0,6,0,0,0,0,1,0); //camera position
//        Matrix.multiplyMM(viewMatrix,0,posemodelMatrix,0,viewMatrix,0);
        Log.d("calib REND VIEWMAT VAL", "start");
        for(float vm:viewMatrix){
            Log.d("calib REND VIEW MAT VAL", String.valueOf(vm));
        }

        Log.d("calib REND VIEWMAT VAL", "end");
        Matrix.setIdentityM(mvpMatrix,0);
        Matrix.setIdentityM(mRotationMatrix,0);
        Matrix.setRotateM(mRotationMatrix, 0, mAngle, 0, 0, -1.0f);
//        Matrix.multiplyMM(mvpMatrix,0,viewMatrix,0,mvpMatrix,0);
        Matrix.multiplyMM(mvpMatrix,0,projectionMatrix,0,viewMatrix,0);
        Matrix.multiplyMM(mvpMatrix,0,mvpMatrix,0,mRotationMatrix,0);
        sampleCube.drawCube2(mvpMatrix);
    }

    public static int loadShader(int type,String shaderCode){
        int shader= GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader,shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }
    public float[] setViewMat(float[] vM){
        Matrix.setIdentityM(posemodelMatrix,0);
        viewMatrix=vM.clone();
        for(float vms:viewMatrix){
            Log.d("calib pose VIEWMAT VAL", String.valueOf(vms));
        }
        return viewMatrix;
    }

    public float getAngle() {
        return mAngle;
    }

    public void setAngle(float angle) {
        mAngle = angle;
    }

    public void setNewTexture(String filename){
//        sampleCube=new Cube2();
        texturefile=filename;
    }

}
