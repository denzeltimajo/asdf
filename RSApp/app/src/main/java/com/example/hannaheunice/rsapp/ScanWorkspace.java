package com.example.hannaheunice.rsapp;

import android.Manifest;
import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class ScanWorkspace extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{
    private static final String TAG="JavaCamera";
    private ScanCameraView jcView;

    private File mImageFolder;
    private File FileData;
    private File croppedPhoto;


    private static String mImageFileName;
    private static final int REQUEST_CAMERA_PERMISSION_RESULT=0;
    private static final int REQUEST_WRITE_PERMISSION_RESULT=1;
    Mat mRgb,mGray;

    private List<Camera.Size> mResolutionPrvList;
    private List<Camera.Size> mResolutionCamList;
    private Camera.Size prvResolution;
    private Camera.Size camResolution;

    Boolean isOver=false;

    static{
        System.loadLibrary("MyOpenCVLibs");

    }

    BaseLoaderCallback mBLCallback=new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            super.onManagerConnected(status);
            switch(status){
                case BaseLoaderCallback.SUCCESS:
                    jcView.enableView();
                    break;
                default:
                    onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_scan_workspace);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        View decorView = getWindow().getDecorView();
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        allowCamera();
        allowWriteStorage();
        createImageFolder();
        createTessDataFolder();
        jcView=(ScanCameraView) findViewById(R.id.javacameraview);

        jcView.setVisibility(SurfaceView.VISIBLE);
        jcView.setCvCameraViewListener(this);

       ImageButton preview = (ImageButton) findViewById(R.id.scan_bt);

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"Loading...",Toast.LENGTH_SHORT).show();
                captureImage();
                isOver=true;


            }
        });
    }

    public void captureImage(){
        try {
            jcView.takePicture(createImageFileName("Orig").getPath());
        }catch (IOException e){
            Log.d(TAG,e.toString());
        }

    }

    private File createImageFileName(String type) throws IOException {
        Log.d(TAG,"CREATEFILENAME");
        String timestamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend="IMG_"+timestamp+"_"+type;
        File imageFile=File.createTempFile(prepend,".png",mImageFolder);
        mImageFileName=imageFile.getAbsolutePath();
        Log.d(TAG,"image filename:"+mImageFileName);
        return imageFile;
    }

    private void allowCamera(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                    PackageManager.PERMISSION_GRANTED){
            }else{
                if(shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)){
                    Toast.makeText(this,"App requires Camera Permission",Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},REQUEST_CAMERA_PERMISSION_RESULT);
            }
        }else{
        }
    }

    private void allowWriteStorage(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED){
            }else{
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this,"App requires Camera Permission",Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_WRITE_PERMISSION_RESULT);
            }
        }else{
        }
    }

    //CREATE FOLDER
    private void createImageFolder(){
        File imageFolder= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        mImageFolder=new File(imageFolder,"RushScan");
        if(!mImageFolder.exists()){
            mImageFolder.mkdirs();
        }
    }

    //CREATE TESS FOLDER
    private void createTessDataFolder(){
        String path = mImageFolder.getAbsolutePath();
        String lang="eng";

        File tessFile=new File(path ,"tessdata");
        if(!tessFile.exists()){
            tessFile.mkdirs();
        }

        if (!(new File(path + "/tessdata/" + lang + ".traineddata")).exists()) {
            try {

                AssetManager assetManager = getAssets();

                InputStream in = assetManager.open(lang + ".traineddata");
                OutputStream out = new FileOutputStream(path
                        + "/tessdata/" + lang + ".traineddata");

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                //while ((lenf = gin.read(buff)) > 0) {
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                //gin.close();
                out.close();
            } catch (IOException e) {
            }
            Log.d("TESSERACT","--ADDED--");
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        Log.i("tost", "WIDTH: "+width);
        Log.i("tost", "height: "+height);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        mResolutionPrvList = jcView.getPreviewResolutionList();
       // mResolutionCamList = jcView.getCameraResolutionList();

        int wid=width;
        int hgt=height;


        ListIterator<Camera.Size> resolutionPrv = mResolutionPrvList.listIterator();
        //ListIterator<Camera.Size> resolutionCam = mResolutionCamList.listIterator();
        int i=0;

        while(resolutionPrv.hasNext()) {
            Camera.Size element = resolutionPrv.next();
            Log.i(TAG, "resolution list: " + element.width + " x " + element.height);
            prvResolution=element;
            wid=element.width;
            hgt=element.height;
        }
/*
        while(resolutionCam.hasNext()) {
            Camera.Size element = resolutionCam.next();
            Log.i(TAG, "resolution list: " + element.width + " x " + element.height);
            camResolution=element;
            wid=element.width;
            hgt=element.height;
        }
*/
        prvResolution.height=displaymetrics.heightPixels;
        prvResolution.width=displaymetrics.widthPixels;

        jcView.setPreviewResolution(prvResolution);
        //jcView.setCameraResolution(camResolution);

        Log.i("tost", "2nd WIDTH: "+wid);
        Log.i("tost", "2nd height: "+hgt);




        //String nn=Environment.getExternalStorageDirectory().getPath();
        //icon=BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath()+"/Pictures/ArkitechImage/doorsorig.png.png");
        mRgb=new Mat(hgt,wid, CvType.CV_8UC4);

        Log.i("tost", "3nd WIDTH: "+mRgb.width());
        Log.i("tost", "3nd height: "+mRgb.height());


        mGray=new Mat(hgt,wid, CvType.CV_8UC1);

    }

    @Override
    public void onCameraViewStopped() {
        mRgb.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgb=inputFrame.rgba();


        if(isOver){

           outProcess();

        }

        return mRgb;
    }

    public void outProcess(){
        OpencvClass.docuScanner(mImageFileName); // return true if success
        Log.d("CROP","START");
        performCrop();
        Log.d("CROP","SUCCESS");

     /* this.stopService(this.getIntent());
//
        Intent i = new Intent(ScanWorkspace.this, Preview.class);
        i.putExtra("file",mImageFileName);
        i.putExtra("folder",mImageFolder);
        isOver=false;
        startActivity(i);
        */
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(jcView!=null){
            jcView.disableView();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(jcView!=null){
            jcView.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(OpenCVLoader.initDebug()){
            Log.d(TAG,"SUCCESSFUL");
            mBLCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }else{
            Log.d(TAG,"UNSUCCESSFUL");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0,this,mBLCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CAMERA_PERMISSION_RESULT){
            if(grantResults[0] !=PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getApplicationContext(),"App will not run without camera permission",Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == REQUEST_WRITE_PERMISSION_RESULT){
            if(grantResults[0] !=PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getApplicationContext(),"App needs storage permission",Toast.LENGTH_SHORT).show();
            }
        }

    }



    public void performCrop() {
        try {
            Log.d("CROP","orig file="+mImageFileName);
            Uri picUri= Uri.fromFile(new File(mImageFileName));
            Log.d("crop","uri="+picUri);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
//            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT,getCropImageUri());
            cropIntent.putExtra("outputFormat",Bitmap.CompressFormat.PNG.toString());
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 2);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (data != null) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
//                Bitmap selectedBitmap = extras.getParcelable("data");
//                Log.d("CROP","height="+selectedBitmap.getHeight());
//                Log.d("CROP","width="+selectedBitmap.getWidth());

                //Save cropped image
//                try{
//                    croppedPhoto=createImageFileName("Crop");
//                    FileOutputStream fos=new FileOutputStream(croppedPhoto);
//                    selectedBitmap.compress(Bitmap.CompressFormat.PNG,90,fos);
//                    fos.close();
//                }catch(IOException e){
//                    e.printStackTrace();
//                }
                //end

                String cropFile=croppedPhoto.getAbsolutePath();
                Log.d("CROP","CROPPED SAVE");
                OpencvClass.scanCrop(cropFile);
                Log.d("CROP","THRESHOLDED");

                this.stopService(this.getIntent());

                Intent i = new Intent(ScanWorkspace.this, Preview.class);
                i.putExtra("file",cropFile);
                i.putExtra("folder",mImageFolder);
                isOver=false;
                startActivity(i);
            }
        }
    }

    public Uri getCropImageUri(){
        //Save cropped image
        try{
            croppedPhoto=createImageFileName("Crop");
//            FileOutputStream fos=new FileOutputStream(croppedPhoto);
//            selectedBitmap.compress(Bitmap.CompressFormat.PNG,90,fos);
//            fos.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        //end
        Uri picUri= Uri.fromFile(new File(croppedPhoto.getAbsolutePath()));
        return picUri;
    }
}
