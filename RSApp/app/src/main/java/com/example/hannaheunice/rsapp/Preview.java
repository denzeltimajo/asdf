package com.example.hannaheunice.rsapp;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.os.PersistableBundle;
import android.media.session.MediaController;
import android.app.SharedElementCallback;
import android.app.assist.AssistContent;
import android.view.SearchEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Preview extends AppCompatActivity {
        View mView;
        ImageView mImageView;
        Button mSave;
        EditText filenameet;
        String filenamevar;
        FloatingActionButton fab, fabsave, fabhighlight, fabshare, fabprint;
        Animation show_fab_1;
        Animation fabOpen, fabClose, fabClockwise, fabAnticlockwise;
        boolean isOpen = false;

        String origfilename;
        String cropFile;
        String filename;
        File filefolder;
        File mPdfFile;
        File croppedPhoto;
        boolean isCropped=false;
        boolean isFileSaved=false;
        boolean isTransparent=false;

        float angle;


        public AlertDialog.Builder mBuilder, mBuilder2;
        public static AlertDialog dialog, dialog2;
        public ListView lv;
        public List<RowItem> rowItems;
        public static final String[] titles = {"Sender","Receiver"};
        public static final Integer[] images = {R.drawable.photocamera_64, R.drawable.gallery_64};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       // Bitmap bm=Bitmap.createBitmap(480,480,Bitmap.Config.ARGB_8888);

        Intent in = this.getIntent();
        origfilename=(String) in.getExtras().get("origfile");
        filename=(String) in.getExtras().get("file");
        filefolder=(File) in.getExtras().get("folder");

        File imgFile = new  File(filename);

        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            mImageView = (ImageView) findViewById(R.id.imageView3);

            mImageView.setImageBitmap(myBitmap);

        }


      //  mImageView.setBackground();

        mView = getLayoutInflater().inflate(R.layout.saveas, null);
        filenameet = (EditText)mView.findViewById(R.id.filename);
        mSave = (Button) mView.findViewById(R.id.save_but); //save button

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabsave = (FloatingActionButton) findViewById(R.id.fab_save);
       // fabhighlight = (FloatingActionButton) findViewById(R.id.fab_highlight);
        fabshare = (FloatingActionButton) findViewById(R.id.fab_share);
        fabprint = (FloatingActionButton) findViewById(R.id.fab_print);


        fabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        fabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        fabAnticlockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);


        rowItems = new ArrayList<RowItem>();
        for (int i = 0; i < titles.length; i++) {
            RowItem item = new RowItem(images[i], titles[i], " ");
            rowItems.add(item);
        }

        lv = new ListView(this);
        CustomBaseAdapter adapter2 = new CustomBaseAdapter(this, rowItems);
        lv.setAdapter(adapter2);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        //sender activity
                        //Toast.makeText(MainActivity.this, "New Photo", Toast.LENGTH_SHORT).show();

                        Intent in = new Intent(Preview.this, Sender.class);
                        in.putExtra("file",filename);
                        startActivity(in);

                        break;
                    case 1:
                        //receiver activity
                        //Toast.makeText(MainActivity.this, "Import", Toast.LENGTH_SHORT).show();

                        Intent intt = new Intent(Preview.this, pulse.class);
                        startActivity(intt);

                        break;
                }
            }
        });

        mBuilder = new AlertDialog.Builder(this);
        mBuilder.setView(lv);

        dialog = mBuilder.create();

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isOpen){
                     //   fabhighlight.startAnimation(fabClose);
                        fabsave.startAnimation(fabClose);
                        fabshare.startAnimation(fabClose);
                        fabprint.startAnimation(fabClose);
                        fab.startAnimation(fabAnticlockwise);
                      //  fabhighlight.setClickable(false);
                        fabsave.setClickable(false);
                        fabshare.setClickable(false);
                        fabprint.setClickable(false);
                        isOpen=false;

                    }
                    else{
                      //  fabhighlight.startAnimation(fabOpen);
                        fabsave.startAnimation(fabOpen);
                        fabshare.startAnimation(fabOpen);
                        fabprint.startAnimation(fabOpen);
                        fab.startAnimation(fabClockwise);
                       // fabhighlight.setClickable(true);
                        fabsave.setClickable(true);
                        fabshare.setClickable(true);
                        fabprint.setClickable(true);
                        isOpen=true;
                    }
                }
            });

        //save as dialog
        filenamevar = filenameet.getText().toString(); // variable nung filename ng isesave
        mBuilder2 = new AlertDialog.Builder(this);
        mBuilder2.setView(mView);

        dialog2 = mBuilder2.create();


        fabsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(Preview.this, "SAVE", Toast.LENGTH_SHORT).show();
                dialog2.show();
            }
        });

        /*fabhighlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTransparent){
                    OpencvClass.addWhite(filename);
                    Log.d("TRANSPARENT","TRUE");
                    isTransparent=false;
                }else{
                    OpencvClass.removeWhite(filename);
                    Log.d("TRANSPARENT","FALSE");
                    isTransparent=true;
                }
//                extractText();
                Toast.makeText(Preview.this, "HIGHLIGHT", Toast.LENGTH_SHORT).show();
            }
        });*/

        fabshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Preview.this, "SHARE", Toast.LENGTH_SHORT).show();
                dialog.show();
            }
        });

        fabprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFileSaved){
                    printDocument();
                    return;
//                    Toast.makeText(Preview.this, "PRINT", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(Preview.this, "Cannot print. File not saved.", Toast.LENGTH_SHORT).show();
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //save
                filenamevar = filenameet.getText().toString(); // variable nung filename ng isesave
                if(changeFileName()==0){
                    dialog2.dismiss();
                    isFileSaved=true;
                }
            }
        });

    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.close, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if(i == R.id.action_cancel){
            Toast.makeText(Preview.this, "CANCEL SCAN GO BACK TO MAIN MENU", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public int changeFileName(){
        // filename = existing
        // filenameet = new
        Log.d("File edit","edited="+filenamevar);
        if(!new File(filefolder,filenamevar+".pdf").exists() || !new File(filefolder,filenamevar+".png").exists()){

            new File(filename).renameTo(new File(filefolder,filenamevar+".png"));
           filename=filefolder+"/"+filenamevar+".png";
            String pdfFilename=filenamevar+".pdf";
            //SAVE PDF
            Document newDocu=new Document();
            try {
                File pdfFile = new File(filefolder, pdfFilename);
                mPdfFile=pdfFile;
                FileOutputStream fo = new FileOutputStream(pdfFile);
                PdfWriter.getInstance(newDocu, fo);
                Log.d("TESSERACT", "Got abs filename: " + pdfFile.getAbsoluteFile());

                newDocu.open();
                Image scannedImage=Image.getInstance(filefolder+"/"+filenamevar+".png");

                scannedImage.setAbsolutePosition(0, 0);
                scannedImage.setBorderWidth(0);
                scannedImage.scaleAbsolute(PageSize.A4);
                newDocu.add(scannedImage);
                newDocu.close();
                fo.close();

                Toast.makeText(Preview.this, "File saved", Toast.LENGTH_SHORT).show();
                filenameet.setText("");
                return 0;
            } catch (DocumentException e) {
                Log.d("TESSERACT","Got file ex");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d("TESSERACT","Got file IO "+e.toString());
                e.printStackTrace();
            }
        }
        Toast.makeText(Preview.this, "File name already exist", Toast.LENGTH_SHORT).show();
        return 1;
    }

    //PRINT SELECTED DOCUMENT
    //SHOULD PASS PDF FILE
    public void printDocument(){
        PrintManager printManager=(PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        String jobName=this.getString(R.string.app_name)+ " Document";
        printManager.print(jobName,new MyPrintDocuAdapter(this,new File(""+mPdfFile)),null);
    }

    public class MyPrintDocuAdapter extends PrintDocumentAdapter {

        Context context;
        File pdfFile;
        private int pageHeight;
        private int pageWidth;
        public PdfDocument selectedPDF;
        public int totalpages;


        public MyPrintDocuAdapter(Context context, File pdfFile) {
            this.context=context;
            this.pdfFile=pdfFile;
            totalpages= computePDFPageCount(pdfFile);
        }

        @Override
        public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {

            selectedPDF= new PrintedPdfDocument(context, newAttributes);
            pageHeight =
                    newAttributes.getMediaSize().getHeightMils()/1000 * 72;
            pageWidth =
                    newAttributes.getMediaSize().getWidthMils()/1000 * 72;

            if (cancellationSignal.isCanceled() ) {
                callback.onLayoutCancelled();
                return;
            }

            if (totalpages > 0) {
                PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                        .Builder("print_output.pdf")
                        .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                        .setPageCount(totalpages);

                PrintDocumentInfo info = builder.build();

                callback.onLayoutFinished(info, true);
            } else {
                callback.onLayoutFailed("Page count is zero.");
            }
        }

        @Override
        public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
            //GET PDF FILE CONTENT
            InputStream pdfInput=null;
            OutputStream pdfOutput=null;
            try{
                pdfInput=new FileInputStream(pdfFile);
                pdfOutput=new FileOutputStream(destination.getFileDescriptor());
                byte[] buf=new byte[1024];
                int bytesRead;

                while((bytesRead= pdfInput.read(buf))>0){
                    pdfOutput.write(buf,0,bytesRead);
                }
                callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES}); // used for selected saved pdf

            } catch (IOException e) {
                e.printStackTrace();
                return;
            } finally {
                try {
                    pdfInput.close();
                    pdfOutput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
                @Override
                public void onCancel() {

                }
            });
        }

        //NEED ITEXT PDF
        public int computePDFPageCount(File file) {
            RandomAccessFile raf = null;
            int pages = 0;
            try {
                raf = new RandomAccessFile(file, "r");

                /* NEEDS ITEXT*/

                RandomAccessFileOrArray pdfFile = new RandomAccessFileOrArray(
                        new RandomAccessSourceFactory().createSource(raf));
                PdfReader reader = new PdfReader(pdfFile, new byte[0]);
                pages = reader.getNumberOfPages();
                reader.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return pages;
        }
    }

    //TESERACT
    public void extractText() {

        Log.d("TESSERACT","--START--");
        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        Log.d("TESSERACT","--tess --");
        String path = filefolder.getAbsolutePath();
        String lang="eng";

        File tessFile=new File(path + "/tessdata/");
        if(!tessFile.exists()){
            tessFile.mkdirs();
        }
        tessBaseAPI.init(path, lang); //Init the Tess with the trained data file, with english language

        Log.d("TESSERACT","cropfile="+filenamevar);
        Bitmap tessbitmap;
        if(filenamevar.equals("")&& isFileSaved){
            Log.d("TESSERACT","filenamevar");
            tessbitmap= BitmapFactory.decodeFile(filefolder+"/"+filenamevar+".png");
        }else{
            tessbitmap= BitmapFactory.decodeFile(filename);
        }

        //CHANGE DPI TO 300+
        tessBaseAPI.setImage(tessbitmap);

        String text = tessBaseAPI.getUTF8Text();
        Log.d("TESSERACT","text extracted="+text);
        tessBaseAPI.end();
        //WRITE TO PDF
        String pdfTempName=filename;
        String[] pdfFileName=pdfTempName.split(Pattern.quote("/"));
        String[] pdfFName=pdfFileName[pdfFileName.length-1].split(Pattern.quote("."));
        Log.d("TESSERACT", "Got filename: " + pdfFName[0]);
        String pdfPath=filefolder.getAbsolutePath()+"/PDF/";
        String pdfname=pdfFName[0]+".pdf";

        Document newDocu=new Document();
        try {
            File pdfTempFile=new File(pdfPath);
            Log.d("TESSERACT", "Got data: " + text);
            if(!pdfTempFile.exists()){
                pdfTempFile.mkdirs();
//                Log.d("TESSERACT", "Got data: " + text);
            }

            File pdfFile=new File(pdfTempFile,pdfname);
            FileOutputStream fo=new FileOutputStream(pdfFile);
            PdfWriter.getInstance(newDocu,fo);
            Log.d("TESSERACT", "Got abs filename: " + pdfFile.getAbsoluteFile());

            newDocu.open();
            newDocu.add(new Paragraph(text));
            newDocu.close();
            fo.close();
            //file size
            double filesize=pdfFile.length()/1000.0;
            String filepath= pdfFile.getAbsolutePath();
            int dotpos= filepath.lastIndexOf(".");
            String fileext=filepath.substring(dotpos+1,filepath.length());
            Log.d("TESSERACT","file size="+filesize);
            Log.d("TESSERACT","file ext="+fileext);
        } catch (DocumentException e) {
            Log.d("TESSERACT","Got file ex");

            e.printStackTrace();
        } catch (IOException e) {
            Log.d("TESSERACT","Got file IO "+e.toString());

            e.printStackTrace();
        }
        Log.d("TESSERACT", "PDF Success! ");
    }

    public void rotateImage(View v){
        angle+=90;
        Bitmap source=BitmapFactory.decodeFile(filename);
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap rotatedImage=Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        mImageView.setImageBitmap(rotatedImage);
//        Toast.makeText(this,"image rotated",Toast.LENGTH_SHORT).show();
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            rotatedImage.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //CROP
    public void performCrop(View v) {
        try {
            Log.d("CROP","orig file="+origfilename);
            Uri picUri= Uri.fromFile(new File(origfilename));
            Log.d("crop","uri="+picUri);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT,getCropImageUri());
            cropIntent.putExtra("outputFormat",Bitmap.CompressFormat.PNG.toString());
            startActivityForResult(cropIntent, 2);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (data != null) {
                // get the returned data
                Bundle extras = data.getExtras();
                isCropped=true;
                cropFile=croppedPhoto.getAbsolutePath();
                Log.d("CROP","CROPPED SAVE");
                OpencvClass.scanCrop(cropFile);
                Log.d("CROP","THRESHOLDED");

                //FIX SOMETHING ON BACK

                Intent i = new Intent(this, Preview.class);
                i.putExtra("origfile",origfilename);
                i.putExtra("file",cropFile);
                i.putExtra("folder",filefolder);
                startActivity(i);
            }
        }
    }

    public Uri getCropImageUri(){
        //Save cropped image
        try{
            croppedPhoto=createImageFileName("Crop");
        }catch(IOException e){
            e.printStackTrace();
        }
        //end
        Uri picUri= Uri.fromFile(new File(croppedPhoto.getAbsolutePath()));
        return picUri;
    }
    public File createImageFileName(String type) throws IOException{
        String fname=origfilename;
        String[] filenameroot=fname.split(Pattern.quote("/"));
        String[] filename=filenameroot[filenameroot.length-1].split(Pattern.quote("."));
        String croppedFilename=filename[0]+"_"+type+".png";
        File crop=new File(filefolder,croppedFilename);
        return crop;
    }
}
