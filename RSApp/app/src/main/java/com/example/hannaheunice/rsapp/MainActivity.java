package com.example.hannaheunice.rsapp;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.os.PersistableBundle;
import android.media.session.MediaController;
import android.widget.Toolbar;
import android.app.SharedElementCallback;
import android.app.assist.AssistContent;
import android.view.SearchEvent;

import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.example.hannaheunice.rsapp.ScanWorkspace;
public class MainActivity extends AppCompatActivity {

    public AlertDialog.Builder mBuilder, mBuilder2;
    public static AlertDialog dialog;
    public AlertDialog dialog2;
    public ListView lv, lv2;
    public List<RowItem> rowItems, rowItems2;
    public static final String[] titles = {"New Photo", "Import Photo"};
    public static final String[] titles2 = {"Sender","Receiver"};
    public static final Integer[] images = {R.drawable.photocamera_64, R.drawable.gallery_64};
    public static final Integer[] images2 = {R.drawable.gallery_64, R.drawable.photocamera_64};
    public Animation animslide, animslide2;
    public ImageView rushimg,scanimg;


    Boolean isOver=false;
    Boolean iscaptured=false;
    File file;
    File mImageFolder;
    private File croppedPhoto;
    public static String mImageFileName;


    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        createImageFolder();

        Button scanbut = (Button)findViewById(R.id.scan_btn);
        Button sharebut = (Button)findViewById(R.id.share_button);
        rushimg = (ImageView)findViewById(R.id.rush_img);
        scanimg = (ImageView)findViewById(R.id.scan_img);
        animslide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide);
        animslide2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide2);

        rushimg.startAnimation(animslide);

        animslide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                scanimg.startAnimation(animslide2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

// Start the animation like this


        View v = getLayoutInflater().inflate(R.layout.activity_main, null);
        v.getBackground().setAlpha(200);
        //scan
        rowItems = new ArrayList<RowItem>();
        for (int i = 0; i < titles.length; i++) {
            RowItem item = new RowItem(images[i], titles[i], " ");
            rowItems.add(item);
        }

        //share
        rowItems2 = new ArrayList<RowItem>();
        for (int i = 0; i < titles.length; i++) {
            RowItem item = new RowItem(images2[i], titles2[i], " ");
            rowItems2.add(item);
        }

        lv = new ListView(this);
        CustomBaseAdapter adapter = new CustomBaseAdapter(this, rowItems);
        lv.setAdapter(adapter);

        mBuilder = new AlertDialog.Builder(this);
        mBuilder.setView(lv);

        dialog = mBuilder.create();

        scanbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent in = new Intent(MainActivity.this,workspace.class);
                // startActivity(in);
                dialog.show();

            }
        });


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        //new photo event
                        //Toast.makeText(MainActivity.this, "New Photo", Toast.LENGTH_SHORT).show();
                     //   Intent in = new Intent(MainActivity.this, ScanWorkspace.class);
                        dialog.dismiss();
                        iscaptured=true;

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        File photo = new File(Environment.getExternalStorageDirectory(),  "Pic.jpg");
                        File photo = null;
                        try {
                            photo = createImageFileName("Orig");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photo));
                        imageUri = Uri.fromFile(photo);
                        startActivityForResult(intent, TAKE_PICTURE);


                        break;
                    case 1:
                        //import photo event
                        //Toast.makeText(MainActivity.this, "Import", Toast.LENGTH_SHORT).show();
//                        File imageFolder= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//                        mImageFolder=new File(imageFolder,"RushScan");
//                        if(!mImageFolder.exists()){
//                            mImageFolder.mkdirs();
//                        }

                        iscaptured=false;

                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, 1);

                        break;
                }
            }
        });


        lv2 = new ListView(this);
        CustomBaseAdapter adapter2 = new CustomBaseAdapter(this, rowItems2);
        lv2.setAdapter(adapter2);

        mBuilder2 = new AlertDialog.Builder(this);
        mBuilder2.setView(lv2);

        dialog2 = mBuilder2.create();



        sharebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "shareee", Toast.LENGTH_SHORT).show();
                /*Intent in = new Intent(MainActivity.this, ScanWorkspace.class);
                startActivity(in);*/

                dialog2.show();
                //dialog2.dismiss();
            }
        });

        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 0:
                        //sender activity
                        Uri seluri = Uri.parse(Environment.getExternalStorageDirectory()
                                + "/Pictures/RushScan/");
                        Intent intent = new Intent(Intent.ACTION_PICK,seluri);
                        //File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Pictures/RushScan/");
//
                        intent.setDataAndType(seluri,"image/*");
                        Intent.createChooser(intent,"RUSHSCAN");
                        startActivityForResult(intent, 3);
//                        startActivity(Intent.createChooser(intent, "Open folder"));

//                        Intent in = new Intent(MainActivity.this, Sender.class);
                        dialog2.dismiss();
//
//                        startActivity(in);

                        break;
                    case 1:
                        //receiver activity
                        //Toast.makeText(MainActivity.this, "Import", Toast.LENGTH_SHORT).show();

                        Intent intt = new Intent(MainActivity.this, pulse.class);
                        dialog2.dismiss();

                        startActivity(intt);

                        break;
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(OpenCVLoader.initDebug()){
            Log.d("OPENCV","SUCCESSFUL");
//            mBLCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }else{
            Log.d("OPENCV","UNSUCCESSFUL");
//            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0,this,mBLCallback);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && iscaptured==true ){

            // String s = getRealPathFromURI(this, selectedImage);
            //String s = Environment.getExternalStorageDirectory().getPath() +"/Pic.jpg";
//            file = new File(Environment.getExternalStorageDirectory(),  "Pic.jpg");;
            String s=mImageFileName;
            OpencvClass.docuScanner(s);
           // OpencvClass.scanCrop(s);
            Intent i = new Intent(MainActivity.this, ImageAdjust.class);
            i.putExtra("file",s);
            i.putExtra("folder",mImageFolder);
            isOver=false;
            startActivity(i);


        //    Toast.makeText(MainActivity.this, "FILE "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();
        //    performCrop(selectedImage);
            //imageDisplay.setImageURI(selectedImage);
            // myimageview.setImageDrawable(new BitmapDrawable(getResources(), mybitmap));
        }
        if (requestCode == 2) {
            if (data != null) {
                // get the returned data
                Bundle extras = data.getExtras();

                String cropFile=croppedPhoto.getAbsolutePath();
                Log.d("CROP","CROPPED SAVE");
                OpencvClass.scanCrop(cropFile);
                Log.d("CROP","THRESHOLDED");

                this.stopService(this.getIntent());

                Intent i = new Intent(MainActivity.this, Preview.class);
                i.putExtra("file",cropFile);
                i.putExtra("folder",mImageFolder);
                isOver=false;
                startActivity(i);
            }
        }
        if (requestCode == 3 &&  resultCode == RESULT_OK && data!=null ){

            Uri selectedImage = data.getData();
            String s = getRealPathFromURI(this, selectedImage);
            file = new File(s);

            Toast.makeText(MainActivity.this, "FILE "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();
            Intent in = new Intent(MainActivity.this, Sender.class);
            in.putExtra("file",s);
            startActivity(in);
        }
        if (requestCode == 1 && iscaptured==false && resultCode == RESULT_OK && data!=null ){

            Uri selectedImage = data.getData();
            String s = getRealPathFromURI(this, selectedImage);
            file = new File(s);

            OpencvClass.docuScanner(s);
            // OpencvClass.scanCrop(s);
            Intent i = new Intent(MainActivity.this, ImageAdjust.class);
            i.putExtra("file",s);
            i.putExtra("folder",mImageFolder);
            isOver=false;
            startActivity(i);
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

//    public void performCrop(Uri picUri) {
//        try {
////            Log.d("CROP","orig file="+mImageFileName);
////            Uri picUri= Uri.fromFile(new File(mImageFileName));
//            Log.d("crop","uri="+picUri);
//            Intent cropIntent = new Intent("com.android.camera.action.CROP");
//            // indicate image type and Uri
//            cropIntent.setDataAndType(picUri, "image/*");
//            // set crop properties here
//            cropIntent.putExtra("crop", true);
//            cropIntent.putExtra("scale", true);
////            cropIntent.putExtra("return-data", true);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT,getCropImageUri());
//            cropIntent.putExtra("outputFormat",Bitmap.CompressFormat.PNG.toString());
//            // start the activity - we handle returning in onActivityResult
//            startActivityForResult(cropIntent, 2);
//        }
//        // respond to users whose devices do not support the crop action
//        catch (ActivityNotFoundException anfe) {
//            // display an error message
//            String errorMessage = "Whoops - your device doesn't support the crop action!";
//            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }
//
//
//    public Uri getCropImageUri(){
//        //Save cropped image
//        try{
//            croppedPhoto=createImageFileName("Crop");
//        }catch(IOException e){
//            e.printStackTrace();
//        }
//        //end
//        Uri picUri= Uri.fromFile(new File(croppedPhoto.getAbsolutePath()));
//        return picUri;
//    }
//
//    public File createImageFileName(String type) throws IOException{
//        String fname=file.getAbsolutePath();
//        String[] filenameroot=fname.split(Pattern.quote("/"));
//        String[] filename=filenameroot[filenameroot.length-1].split(Pattern.quote("."));
//        String croppedFilename=filename[0]+"_"+type+".png";
//        File crop=new File(mImageFolder,croppedFilename);
//        return crop;
//    }

    private File createImageFileName(String type) throws IOException {
        Log.d("filename","CREATEFILENAME");
        String timestamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend="IMG_"+timestamp+"_"+type;
        File imageFile=File.createTempFile(prepend,".png",mImageFolder);
        mImageFileName=imageFile.getAbsolutePath();
        Log.d("filename","image filename:"+mImageFileName);
        return imageFile;
    }
    //CREATE FOLDER
    private void createImageFolder(){
        File imageFolder= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        mImageFolder=new File(imageFolder,"RushScan");
        if(!mImageFolder.exists()){
            mImageFolder.mkdirs();
        }
    }
}
