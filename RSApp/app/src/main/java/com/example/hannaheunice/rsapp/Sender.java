package com.example.hannaheunice.rsapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Sender extends AppCompatActivity {
    //CLIENT VARS
    private WifiP2pManager wifiManag;
    private WifiP2pManager.Channel wifichannel;

    private File fileToSend;

    private BroadcastReceiver wifiClientReceiver;
    private IntentFilter wifiClientReceiverIntentFilter;
    private Intent clientServiceIntent;

    private WifiP2pDevice targetDevice;
    private WifiP2pInfo wifiInfo;
    private boolean connectedAndReadyToSendFile;

    private boolean isGroupOwn=false;
    private String groupIp="";

    public final int port = 7950;

    //PEER RECIEVER VARS
    private WifiP2pManager mP2PManager;
    private WifiP2pManager.Channel mChannel;



    private String devName;
    private String devAddr;
    private boolean isNameSelected=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

      //  Log.i("tost","IP ADD: - " + getIpAddressFromArpCache("e4:58:e7:87:ea:2f"));

        android.net.wifi.WifiManager wifi=(android.net.wifi.WifiManager) getSystemService(Context.WIFI_SERVICE);


        wifiManag = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);

        wifichannel = wifiManag.initialize(this, getMainLooper(), null);

        wifiClientReceiver = new WiFiClientBroadcastReceiver(wifiManag, wifichannel, this);

        wifiClientReceiverIntentFilter = new IntentFilter();;
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        //wifiClientReceiverIntentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

        registerReceiver(wifiClientReceiver, wifiClientReceiverIntentFilter);

        while(!wifi.isWifiEnabled()){
            Log.i("tost","wifi peers wifi enabled");
            wifi.setWifiEnabled(true);
        }

        Intent in = this.getIntent();
        String prepend = (String) in.getExtras().get("file");

        fileToSend = new File(prepend);


        mP2PManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel=mP2PManager.initialize(this,getMainLooper(),null);

        mP2PManager.discoverPeers(this.mChannel,new WifiP2pManager.ActionListener(){

            @Override
            public void onSuccess() {

                Log.i("tost","wifi peers discover success");
            }

            @Override
            public void onFailure(int reason) {
                Log.i("tost","wifi peers discover failed");

            }
        });

        wifiManag.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {

            public void onPeersAvailable(WifiP2pDeviceList peers) {

                displayPeers(peers);

            }
        });

        FloatingActionButton search=(FloatingActionButton) findViewById(R.id.fab);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // disconnect();
                mP2PManager.discoverPeers(mChannel,null);
                wifiManag.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {

                    public void onPeersAvailable(WifiP2pDeviceList peers) {

                        displayPeers(peers);

                    }
                });

                // android.net.wifi.WifiManager wifi=(android.net.wifi.WifiManager) getSystemService(Context.WIFI_SERVICE);
               // Log.i("CLIENT_AP", Formatter.formatIpAddress(wifi.getDhcpInfo().serverAddress));
               // displayWifi(wifi.getScanResults());
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            backPressed();
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }


    public void backPressed() {
        stopClientReceiver();
        disconnect();
    }

    public void setGroupInfo(WifiP2pGroup group){
        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
        Collection<WifiP2pDevice> totinos =group.getClientList();
        WifiP2pDevice[] tort=totinos.toArray(new WifiP2pDevice[totinos.size()]);
        groupIp=getIpAddressFromArpCache(tort[0].deviceAddress);
        Log.i("tost","IP ADD: - " + groupIp );
        isGroupOwn=true;
        sendFile();
    }

    public static String getIpAddressFromArpCache(String macAddr) {
        String ip="op";
        if (macAddr == null)
            return null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                line=br.readLine();
                String[] splitted = line.split(" +");
                boolean x1=splitted != null;
                boolean x2=splitted.length >= 4;
                boolean x3=macAddr.equals(splitted[3]);
              // if (splitted != null && splitted.length >= 4 && macAddr.equals(splitted[3])) {
                    // Basic sanity check
                    ip = splitted[0];
                   // if (ip.matches("..:..:..:..:..:..")) {
                    return ip;
                    //} else {
                    //    return null;
                    //}
             //   }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ip;
    }

    //PEER FUNCTIONS
    public void displayPeers(final WifiP2pDeviceList peerList){
        ListView peerView=(ListView) findViewById(R.id.listView);
        ArrayList<String> peersStringArrayList = new ArrayList<String>();

        for(WifiP2pDevice wd:peerList.getDeviceList()){
            peersStringArrayList.add(wd.deviceName);
        }

        peerView.setClickable(true);

        ArrayAdapter arrayAdapter=new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,peersStringArrayList.toArray());
        peerView.setAdapter(arrayAdapter);
        Log.d("WIFI connect","wifi peers array adapter");
        peerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isNameSelected=true;
                TextView txtVw=(TextView) view;

                WifiP2pDevice selectedDevice=null;

                for(WifiP2pDevice wd: peerList.getDeviceList()){
                    if(wd.deviceName.equals(txtVw.getText())){
                        selectedDevice=wd;
                    }
                }

                devName=selectedDevice.deviceName;
                devAddr=selectedDevice.deviceAddress;
                Log.i("tost",selectedDevice.deviceAddress);
                if(selectedDevice!=null){
                    connectToPeer(selectedDevice);
                    Log.i("tost","wifi peers connected to "+selectedDevice.deviceName);
                    //connect to device;

                }else{
                    Log.d("tost","wifi peers not connected");
                }
                //sendFile();
            }
        });
    }

    public void disconnect() {
        Log.i("tost", "wifi peers removeGroup ");
        if (mP2PManager != null && mChannel != null) {
            //Log.d("tost", "wifi peers removeGroup ");
            mP2PManager.requestGroupInfo(mChannel, new WifiP2pManager.GroupInfoListener() {
                @Override
                public void onGroupInfoAvailable(final WifiP2pGroup group) {
                    if (group != null && mP2PManager != null && mChannel != null
                            && group.isGroupOwner()) {
                        Log.d("tost", "wifi peers removeGroup info and group availlable");
                        mP2PManager.removeGroup(mChannel, new WifiP2pManager.ActionListener() { //disconnect from client

                            @Override
                            public void onSuccess() {

                                Log.i("tost", "wifi peers removeGroup onSuccess -"+group.getNetworkName());
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.i("tost", "wifi peers removeGroup onFailure -" + reason);
                            }
                        });
                    }
                }
            });
        }
        android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager)getSystemService(Context.WIFI_SERVICE);
        wifiManager.disconnect();
        wifiManager.setWifiEnabled(false);
    }

    public void connectToPeer(WifiP2pDevice selectedDevice) {

        WifiP2pConfig config = new WifiP2pConfig();
        config.groupOwnerIntent = 0;
        config.deviceAddress = selectedDevice.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        mP2PManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(Sender.this, "Connection success!",
                        Toast.LENGTH_SHORT).show();
                if(isNameSelected){
                    //sendFile();
                }
                isNameSelected=false;
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(Sender.this, "Connect failed. Retry.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //END OF PEER FUNCTIONS

    public void setNetworkToReadyState(boolean status, WifiP2pInfo info, WifiP2pDevice device)
    {
        wifiInfo = info;
        targetDevice = device;
        connectedAndReadyToSendFile = status;
    }

    public void setTransferStatus(boolean status)
    {
        connectedAndReadyToSendFile = status;
    }

    public void sendFile() {

        if(!connectedAndReadyToSendFile)
        {
            Log.i("tost","You must be connected to a server before attempting to send a file");
        }
/*
        else if(targetDevice == null)
       {
	        	Log.i("tost","Target Device network information unknown");
       }
*/
        else if(wifiInfo == null)
            {
                Log.i("tost","Missing Wifi P2P information");
            }
        else if(groupIp.equals("op")){
            Log.i("tost","no");
        }
        else
        {
            //Launch client service
            clientServiceIntent = new Intent(this, ClientService.class);
            clientServiceIntent.putExtra("fileToSend", fileToSend);
            clientServiceIntent.putExtra("port", new Integer(port));
            //clientServiceIntent.putExtra("targetDevice", targetDevice);
            clientServiceIntent.putExtra("wifiInfo", wifiInfo);
            clientServiceIntent.putExtra("isGroup", isGroupOwn);
            clientServiceIntent.putExtra("groupAddr", groupIp);
            clientServiceIntent.putExtra("devName", devName);
            clientServiceIntent.putExtra("devAddr", devAddr);
            clientServiceIntent.putExtra("clientResult", new android.os.ResultReceiver(null) {
                @Override
                protected void onReceiveResult(int resultCode, final Bundle resultData) {

                    if(resultCode == port )
                    {
                        if (resultData == null) {
                            //Client service has shut down, the transfer may or may not have been successful. Refer to message
                            //transferActive = false;
                        }
                        else
                        {
                               /* final TextView client_status_text = (TextView) findViewById(R.id.file_transfer_status);

                                client_status_text.post(new Runnable() {
                                    public void run() {
                                        client_status_text.setText((String)resultData.get("message"));
                                    }
                                });*/
                        }
                    }

                }
            });

            //transferActive = true;
            startService(clientServiceIntent);



            //end
        }

}

    protected void onDestroy() {
        super.onDestroy();
        Log.d("WIFI connect","wifi peers ON DESTROY");
//        unregisterReceiver(mReceiver);
        stopClientReceiver();
        disconnect();
        Log.d("WIFI connect","wifi peers END ON DESTROY");
    }
    private void stopClientReceiver()
    {
        try
        {
            unregisterReceiver(wifiClientReceiver);
        }
        catch(IllegalArgumentException e)
        {
            //This will happen if the server was never running and the stop button was pressed.
            //Do nothing in this case.
        }
    }

}
