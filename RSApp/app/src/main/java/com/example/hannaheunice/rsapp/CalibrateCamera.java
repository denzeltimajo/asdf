package com.example.hannaheunice.rsapp;


/**
 * Created by carizanatividad on 1/11/17.
 */

public class CalibrateCamera {
    public native static boolean canCalibrate(long frameAddress,String path);
    public native static void calibrateSave(long frameAddress, String path, float[][] imgpts, float[][] objpts);
    public native static float[] augment(long frameAddress,String path);
    public native static float[][] getImagePoints(long frameAddress, float[][] imgpts, int snap);
    public native static float[][] getObjectPoints(long frameAddress, float[][] objPoints, int snap);
    public native static float[] augmentUsingContours(long frameAddress, String path);
}
