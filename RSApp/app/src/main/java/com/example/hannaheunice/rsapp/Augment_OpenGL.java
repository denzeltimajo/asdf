package com.example.hannaheunice.rsapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class Augment_OpenGL extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG="JavaCamera";
    private ScanCameraView jcView;
    private ImageButton captureButton;
    private File mImageFolder;
    private String mImageFileName;
    private static final int REQUEST_CAMERA_PERMISSION_RESULT=0;
    private static final int REQUEST_WRITE_PERMISSION_RESULT=1;
    Mat mRgb,mGray,thresholded;
    boolean augmentbuttonclicked=false;

    boolean isTouched=false;
    boolean isTransparent=false;
    static String filename;


    public String root;

    private GLSurfaceView glView;
    private MyGLRenderer glRendeder;

    private List<Camera.Size> mResolutionPrvList;
    private List<Camera.Size> mResolutionCamList;
    private Camera.Size prvResolution;
    private Camera.Size camResolution;


    SwitchCompat togglebut;

    int calibrate_count=0;
    int snapshot_cnt=10;
    private float[][] temp_imgpts;
    private float[][] temp_objpts;
    private float[][] save_imgpts;
    private float[][] save_objpts;

    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousX;
    private float mPreviousY;

    static{
        System.loadLibrary("MyOpenCVLibs");

    }
    BaseLoaderCallback mBLCallback=new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            super.onManagerConnected(status);
            switch(status){
                case BaseLoaderCallback.SUCCESS:
                    jcView.enableView();
                    break;
                default:
                    onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_augment__open_gl);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        allowCamera();
        allowWriteStorage();

        Log.d("GLSU","JC");

        jcView=(ScanCameraView) findViewById(R.id.javacameraview);
        jcView.setVisibility(SurfaceView.VISIBLE);
        jcView.setCvCameraViewListener(this);


        Intent in = this.getIntent();
//        filename= Environment.getExternalStorageDirectory().getPath() + "/Pictures/RushScan/artoolkit.png";
        filename = (String) in.getExtras().get("filename");
        filename=Environment.getExternalStorageDirectory().getPath()+"/Pictures/RushScan/"+filename;
//
        glView=new GLSurfaceView(this);
        glView.setEGLContextClientVersion(2);
        glView.setZOrderOnTop(true);
        glView.setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
        glView.getHolder().setFormat( PixelFormat.TRANSLUCENT);
        glView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        jcView.getLayoutParams())
                        );
        Log.d("GLSU","NN");
        addContentView(glView,glView.getLayoutParams());
        glRendeder=new MyGLRenderer(this);
        glView.setRenderer(glRendeder);
        glView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        //View mView = getLayoutInflater().inflate(R.layout.on_off_switch, null); test
        togglebut = (SwitchCompat) findViewById(R.id.switchForActionBar);

        togglebut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(Augment_OpenGL.this, "Nako "+b, Toast.LENGTH_LONG).show();
                    if (b && isTransparent==false){
                        Log.d("TOGGLE","TOGGLE IS ON");
                        OpencvClass.removeWhite(filename);
                        isTransparent=true;
//                        Toast.makeText(Augment_OpenGL.this, "NAKA ON", Toast.LENGTH_SHORT).show();
                    }else if(b==false && isTransparent==true){
                        Log.d("TOGGLE","TOGGLE IS OFF");
                        OpencvClass.addWhite(filename);
                        isTransparent=false;
                    }
                    glView.onPause();
                    String rendfile=filename;
                    glRendeder.setNewTexture(rendfile);
                    glView.requestRender();
                    glView.onResume();
                }

        });
    }


    //ROTATION FUNCTION ONSCREEN
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();

        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:

                float dx = x - mPreviousX;
                float dy = y - mPreviousY;
                // reverse direction of rotation above the mid-line
                if (y > glView.getHeight() / 2) {
//                    dx = dx * -1 ;
                    dy = dy * -1;
                }
                // reverse direction of rotation to left of the mid-line
                if (x < glView.getWidth() / 2) {
//                    dy = dy * -1;
                    dx = dx * -1 ;

                }
                glRendeder.setAngle(
                        glRendeder.getAngle() +
                                ((dx + dy) * TOUCH_SCALE_FACTOR));
                glView.requestRender();
        }
        mPreviousX = x;
        mPreviousY = y;
        return true;
    }
    //END

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(jcView!=null){
            jcView.disableView();
            if(isTransparent==true) {
                Log.d("TOGGLE", "TOGGLE IS OFF");
                OpencvClass.addWhite(filename);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(jcView!=null){
            jcView.disableView();
        }
        if(glView != null) {
            glView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(glView != null) {
            glView.onResume();
        }
        if(OpenCVLoader.initDebug()){
            Log.d(TAG,"SUCCESSFUL");
            mBLCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }else{
            Log.d(TAG,"UNSUCCESSFUL");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0,this,mBLCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CAMERA_PERMISSION_RESULT){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getApplicationContext(),"App will not run without camera permission", Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == REQUEST_WRITE_PERMISSION_RESULT){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getApplicationContext(),"App needs storage permission", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void allowCamera(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                    PackageManager.PERMISSION_GRANTED){
            }else{
                if(shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)){
                    Toast.makeText(this,"App requires Camera Permission", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},REQUEST_CAMERA_PERMISSION_RESULT);
            }
        }else{
        }
    }
    private void allowWriteStorage(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED){
                    File root0 =  android.os.Environment.getExternalStorageDirectory();;
                    root=root0.toString();
            }else{
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this,"App requires Camera Permission", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_WRITE_PERMISSION_RESULT);
            }
        }else{
        }
    }

    static{
        if(OpenCVLoader.initDebug()){
            Log.d(TAG,"SUCCESSFUL");
        }else{
            Log.d(TAG,"UNSUCCESSFUL");
        }
    }


    @Override
    public void onCameraViewStarted(int width, int height) {
        Log.i("tost", "WIDTH: "+width);
        Log.i("tost", "height: "+height);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        mResolutionPrvList = jcView.getPreviewResolutionList();
        mResolutionCamList = jcView.getCameraResolutionList();

        int wid=width;
        int hgt=height;


        ListIterator<Camera.Size> resolutionPrv = mResolutionPrvList.listIterator();
        ListIterator<Camera.Size> resolutionCam = mResolutionCamList.listIterator();
        int i=0;

        while(resolutionPrv.hasNext()) {
            Camera.Size element = resolutionPrv.next();
            Log.i(TAG, "resolution list: " + element.width + " x " + element.height);
            prvResolution=element;
            wid=element.width;
            hgt=element.height;
        }

        while(resolutionCam.hasNext()) {
            Camera.Size element = resolutionCam.next();
            Log.i(TAG, "resolution list: " + element.width + " x " + element.height);
            camResolution=element;
            wid=element.width;
            hgt=element.height;
        }

        prvResolution.height=displaymetrics.heightPixels;
        prvResolution.width=displaymetrics.widthPixels;

        jcView.setPreviewResolution(prvResolution);
        jcView.setCameraResolution(camResolution);

        Log.i("tost", "2nd WIDTH: "+wid);
        Log.i("tost", "2nd height: "+hgt);

        mRgb=new Mat(height,width, CvType.CV_8UC4);
        mGray=new Mat(height,width, CvType.CV_8UC1);
        thresholded=new Mat(height,width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mRgb.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgb=inputFrame.rgba();

//        Runnable r = new Runnable(){
//            @Override
//            public void run() {
//                boolean canCali=CalibrateCamera.canCalibrate(mRgb.getNativeObjAddr(),root);
//                if(canCali){
                    Log.d("CALIBRATION","can calibrate");
                    float[] viewMat=CalibrateCamera.augmentUsingContours(mRgb.getNativeObjAddr(),root);
                    if(viewMat.length==0){

                    }else {
                        int stream = 0;
                        for (float vm : viewMat) {
                            Log.d("calib VIEWMAT VAL", String.valueOf(vm));
                        }
                        Log.d("CALIBRATION", "end viewmat");
                        glRendeder.setViewMat(viewMat);
                        glView.requestRender();
                        Log.d("CALIBRATION", "end view mat");
                    }
//                }else{
//            Toast.makeText(getApplicationContext(),"DONT!!",Toast.LENGTH_SHORT);
                    Log.d("CALIBRATION","cant calibrate");
//                glView.requestRender();
//                }
//            }
//        };

//        Thread sampleThread=new Thread(r);
//        sampleThread.start();

        Log.d("CALIBRATION","calibrated imgs count="+calibrate_count);
        return mRgb;
    }

    public void snapshot(View v){
        boolean canCalib;
        canCalib=CalibrateCamera.canCalibrate(mRgb.getNativeObjAddr(),root);
        float img;
        if(canCalib){
            temp_imgpts=CalibrateCamera.getImagePoints(mRgb.getNativeObjAddr(),save_imgpts,calibrate_count);
            temp_objpts=CalibrateCamera.getObjectPoints(mRgb.getNativeObjAddr(),save_objpts,calibrate_count);
            for(float[] imgs:temp_imgpts){
                Log.d("POINTS img temp", Arrays.toString(imgs));
            }
            //**DEBUG**//
            Log.d("POINTS img","temp_imgpts sizze="+ temp_imgpts[0].length);
            save_imgpts=new float[temp_imgpts.length][temp_imgpts[0].length];
            for(int i=0;i<temp_imgpts.length;i++){
                for(int j=0;j<temp_imgpts[i].length;j++){
                    save_imgpts[i][j]=temp_imgpts[i][j];
                }
            }
            Log.d("POINTS img saved", "saved");
            for(float[] imgs:save_imgpts){
                Log.d("POINTS img saved", Arrays.toString(imgs));
            }
            Log.d("POINTS obj","temp_objpts sizze="+ temp_objpts[0].length);
            save_objpts=new float[temp_objpts.length][temp_objpts[0].length];
            for(int i=0;i<temp_objpts.length;i++){
                for(int j=0;j<temp_objpts[i].length;j++){
                    save_objpts[i][j]=temp_objpts[i][j];
                }
            }
            Log.d("POINTS obj saved", "saved");
            for(float[] imgs:save_objpts){
                Log.d("POINTS obj saved", Arrays.toString(imgs));
            }
            //**END DEBUG**//
            calibrate_count++;
//            if(calibrate_count==10){
                CalibrateCamera.calibrateSave(mRgb.getNativeObjAddr(),root,save_imgpts,save_objpts);
//            }
        }
    }

    public void augment(View v){
        float[] viewMat=CalibrateCamera.augment(mRgb.getNativeObjAddr(),root);
        for(float vm:viewMat){
            Log.d("calib ag VIEWMAT VAL", String.valueOf(vm));
        }
        glRendeder.setViewMat(viewMat);
        glView.requestRender();
    }
}
