package com.example.hannaheunice.rsapp;

/**
 * Created by carizanatividad on 1/1/17.
 */
public class OpencvClass {
    static{
        System.loadLibrary("MyOpenCVLibs");
    }
    public native static int convertGray(long mataddrRgba, long mataddrGray);
    public native static void docuScanner(String filePath);
    public native static void scanCrop(String filePath);
    public native static void removeWhite(String filePath);
    public native static void addWhite(String filePath);
}
