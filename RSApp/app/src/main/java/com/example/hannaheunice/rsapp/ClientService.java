package com.example.hannaheunice.rsapp;

/**
 * Created by john doe on 2/3/2017.
 */

import android.app.IntentService;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientService extends IntentService {

    private boolean serviceEnabled;

    private int port;
    private File fileToSend;
    private ResultReceiver clientResult;
    private WifiP2pDevice targetDevice;
    private WifiP2pInfo wifiInfo;
    private String mGroup;

    public ClientService() {
        super("ClientService");
        serviceEnabled = true;

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        port = ((Integer) intent.getExtras().get("port")).intValue();
        fileToSend = (File) intent.getExtras().get("fileToSend");
        clientResult = (ResultReceiver) intent.getExtras().get("clientResult");
        //targetDevice = (WifiP2pDevice) intent.getExtras().get("targetDevice");
        wifiInfo = (WifiP2pInfo) intent.getExtras().get("wifiInfo");
        mGroup = (String) intent.getExtras().get("groupAddr");


        if(!wifiInfo.isGroupOwner)
        {
            //IP OF RECIEVER -- as CLIENT
            InetAddress targetIP = wifiInfo.groupOwnerAddress;


            Log.d("tost","ip "+targetIP);
            Socket clientSocket = null;
            OutputStream os = null;

            try {

                clientSocket = new Socket(targetIP, port);

                byte[] mybytearray = new byte[(int) fileToSend.length()];

                FileInputStream fis = new FileInputStream(fileToSend);
                BufferedInputStream bis = new BufferedInputStream(fis);
                //bis.read(mybytearray, 0, mybytearray.length);

                DataInputStream dis = new DataInputStream(bis);
                dis.readFully(mybytearray, 0, mybytearray.length);

                os = clientSocket.getOutputStream();
                //    os.write(fileToSend.getName().getBytes());


                DataOutputStream dos = new DataOutputStream(os);
                dos.writeUTF(fileToSend.getName());
                dos.writeLong(mybytearray.length);
                dos.write(mybytearray, 0, mybytearray.length);
                dos.flush();

                os.write(mybytearray, 0, mybytearray.length);

//                PrintWriter pw = new PrintWriter(os);
//
//                InputStream is = clientSocket.getInputStream();
//                InputStreamReader isr = new InputStreamReader(is);
//                BufferedReader br = new BufferedReader(isr);
//
//                signalActivity("About to start handshake");
//
//
//
//                //Client-Server handshake
//				/*
//				pw.println(fileToSend.getName());
//
//
//
//				String inputData = "";
//
//				pw.println("wdft_client_hello");
//
//				inputData = br.readLine();
//
//				if(!inputData.equals("wdft_server_hello"))
//				{
//					throw new IOException("Invalid WDFT protocol message");
//
//				}
//
//
//				pw.println(fileToSend.getName());
//
//				if(!inputData.equals("wdft_server_ready"))
//				{
//					throw new IOException("Invalid WDFT protocol message");
//
//				}
//
//				*/
//
//                //Handshake complete, start file transfer
//
//
//
//                byte[] buffer = new byte[4096];
//
//                FileInputStream fis = new FileInputStream(fileToSend);
//                BufferedInputStream bis = new BufferedInputStream(fis);
//                // long BytesToSend = fileToSend.length();
//
//                while(true)
//                {
//
//                    int bytesRead = bis.read(buffer, 0, buffer.length);
//
//                    if(bytesRead == -1)
//                    {
//                        break;
//                    }
//
//                    //BytesToSend = BytesToSend - bytesRead;
//                    os.write(buffer,0, bytesRead);
//                    os.flush();
//                }
//
//
//
//                fis.close();
//                bis.close();
//
//                br.close();
//                isr.close();
//                is.close();
//
//                pw.close();
                os.close();
                dos.close();
                clientSocket.close();


                Toast.makeText(this, "Transfer Complete, sent file as Client, File:"  + fileToSend.getName(),
                        Toast.LENGTH_SHORT).show();
                signalActivity("File Transfer Complete, sent file: " + fileToSend.getName());


            } catch (IOException e) {
                signalActivity(e.getMessage());
            }
            catch(Exception e)
            {
                signalActivity(e.getMessage());

            }

        }
        else
        {

            //IP OF RECIEVER -- as GROUP OWNER
            InetAddress targetIP = null;
            try {
                targetIP = InetAddress.getByAddress(ipStringToByteArray(mGroup));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }


            // Log.d("tost","ip "+targetIP);
            Socket clientSocket = null;
            OutputStream os = null;

            try {

                clientSocket = new Socket(targetIP, port);

                byte[] mybytearray = new byte[(int) fileToSend.length()];

                FileInputStream fis = new FileInputStream(fileToSend);
                BufferedInputStream bis = new BufferedInputStream(fis);
                //bis.read(mybytearray, 0, mybytearray.length);

                DataInputStream dis = new DataInputStream(bis);
                dis.readFully(mybytearray, 0, mybytearray.length);

                os = clientSocket.getOutputStream();
            //    os.write(fileToSend.getName().getBytes());


                DataOutputStream dos = new DataOutputStream(os);
                dos.writeUTF(fileToSend.getName());
                dos.writeLong(mybytearray.length);
                dos.write(mybytearray, 0, mybytearray.length);
                dos.flush();

                os.write(mybytearray, 0, mybytearray.length);

//                PrintWriter pw = new PrintWriter(os);
//
//                InputStream is = clientSocket.getInputStream();
//                InputStreamReader isr = new InputStreamReader(is);
//                BufferedReader br = new BufferedReader(isr);
//
//                signalActivity("About to start handshake");
//
//
//
//                //Client-Server handshake
//				/*
//				pw.println(fileToSend.getName());
//
//
//
//				String inputData = "";
//
//				pw.println("wdft_client_hello");
//
//				inputData = br.readLine();
//
//				if(!inputData.equals("wdft_server_hello"))
//				{
//					throw new IOException("Invalid WDFT protocol message");
//
//				}
//
//
//				pw.println(fileToSend.getName());
//
//				if(!inputData.equals("wdft_server_ready"))
//				{
//					throw new IOException("Invalid WDFT protocol message");
//https://gitlab.com/denzeltimajo/thesis.git
//				}
//
//				*/
//
//                //Handshake complete, start file transfer
//
//
//
//                byte[] buffer = new byte[4096];
//
//                FileInputStream fis = new FileInputStream(fileToSend);
//                BufferedInputStream bis = new BufferedInputStream(fis);
//                // long BytesToSend = fileToSend.length();
//
//                while(true)
//                {
//
//                    int bytesRead = bis.read(buffer, 0, buffer.length);
//
//                    if(bytesRead == -1)
//                    {
//                        break;
//                    }
//
//                    //BytesToSend = BytesToSend - bytesRead;
//                    os.write(buffer,0, bytesRead);
//                    os.flush();
//                }
//
//
//
//                fis.close();
//                bis.close();
//
//                br.close();
//                isr.close();
//                is.close();
//
//                pw.close();
                os.close();
                dos.close();
                clientSocket.close();


                Toast.makeText(this, "Transfer Complete, sent file as Owner, File:"  + fileToSend.getName(),
                        Toast.LENGTH_SHORT).show();
                //signalActivity("File Transfer Complete, sent file: " + fileToSend.getName());


            } catch (IOException e) {
                signalActivity(e.getMessage());
            }
            catch(Exception e)
            {
                signalActivity(e.getMessage());

            }
        }


        clientResult.send(port, null);
    }


    public void signalActivity(String message)
    {
        Bundle b = new Bundle();
        b.putString("message", message);
        clientResult.send(port, b);
    }


    public void onDestroy()
    {
        serviceEnabled = false;

        //Signal that the service was stopped
        //serverResult.send(port, new Bundle());

        stopSelf();
    }

    public static byte[] ipStringToByteArray(String s) {
        int len = s.length();
        String[] splitted=s.split("\\.");
        byte[] data = new byte[4];
        for (int i = 0; i < 4; i ++) {
            int a=Integer.parseInt(splitted[i]);
            data[i]=(byte) a;
//            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
//                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }



}