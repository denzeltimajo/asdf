package com.example.hannaheunice.rsapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Environment;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static android.opengl.GLES20.GL_TEXTURE_2D;
//import com.example.carizanatividad.javacameraviewopencv.JavaCamera;

/**
 * Created by carizanatividad on 1/7/17.
 */

public class Cube2 {
    private final int mProgram;
    private FloatBuffer vertexBuffer;
    private FloatBuffer textureBuffer;
    private ShortBuffer drawListBuffer;

    Mat image;

    private final String vertexshadercode=
            "uniform mat4 uMVPmatrix; "+
                    "attribute vec2 a_TexCoordinate;" +
                    "varying vec2 v_TexCoordinate;" +
                    "attribute vec4 vPosition;" +
                    "void main(){"+
                    " gl_Position= uMVPmatrix*vPosition;"+
                    " v_TexCoordinate = a_TexCoordinate;" +
                    "}";
    private final String fragmentshadercode=
            "precision mediump float;"
                    +"uniform vec4 v_color;"+
                    "varying vec2 v_TexCoordinate;" +
                    "uniform sampler2D s_Texture;"
                    +" void main(){"
                    +" gl_FragColor= (texture2D(s_Texture,v_TexCoordinate));"
                    +"}";

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float squareCoords[] = {
            -1.0f, -1.0f, 0.0f,  // 0. left-bottom-front
            1.0f, -1.0f, 0.0f,  // 1. right-bottom-front
            -1.0f,  1.0f, 0.0f,  // 2. left-top-front
            1.0f,  1.0f, 0.0f   // 3. right-top-front
    }; // top right

    private short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices
    float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };
    float[] mCurrentModelMatrix = new float[16];
    private float[][] colors = {  // Colors of the 6 faces
            {1.0f, 0.5f, 0.0f, 1.0f},  // 0. orange
//            {1.0f, 0.0f, 1.0f, 1.0f},  // 1. violet
//            {0.0f, 1.0f, 0.0f, 1.0f},  // 2. green
//            {0.0f, 0.0f, 1.0f, 1.0f},  // 3. blue
//            {1.0f, 0.0f, 0.0f, 1.0f},  // 4. red
//            {1.0f, 1.0f, 0.0f, 1.0f}   // 5. yellow
    };
    private float[] texCoords = { // Texture coords for the above face
            0.0f, 0.0f,  // A. left-bottom
            1.0f, 0.0f,  // B. right-bottom
            0.0f, 1.0f,  // C. left-top
            1.0f, 1.0f   // D. right-top
    };

    private int vpositionHandle;
    private int textureHandle[]=new int[1];
    private int textUniformHandle;
    private int textCoordHandle;
    private int fcolorHandle;
    private int mvpHandle;
    private int vertex_cnt=squareCoords.length/COORDS_PER_VERTEX;
    private int vertex_stride=COORDS_PER_VERTEX*4;

    public Cube2() {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                squareCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(squareCoords);
        vertexBuffer.position(0);


        ByteBuffer textBuffer = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                texCoords.length * 4);
        textBuffer.order(ByteOrder.nativeOrder());
        textureBuffer = textBuffer.asFloatBuffer();
        textureBuffer.put(texCoords);
        textureBuffer.position(0);


        int vertexShader=MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,vertexshadercode);
        int fragmentShader=MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,fragmentshadercode);

        mProgram= GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram,vertexShader);
        GLES20.glAttachShader(mProgram,fragmentShader);
        GLES20.glBindAttribLocation(mProgram, 0, "a_TexCoordinate");

        GLES20.glLinkProgram(mProgram);
        loadTexture();
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

    }

    public void loadTexture(){
        String mfile;
        if(MyGLRenderer.texturefile==null){
//            mfile=Environment.getExternalStorageDirectory().getPath() + "/Pictures/RushScan/artoolkit.png";
            mfile=Augment_OpenGL.filename;
            Log.d("png cube","TOGGLE doesnt change");
        }else{
            Log.d("png cube photo ","TOGGLE = "+MyGLRenderer.texturefile);
            mfile=MyGLRenderer.texturefile;
        }

        BitmapFactory.Options bmOptions=new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds=false;
        Bitmap photoCaptured= BitmapFactory.decodeFile(mfile,bmOptions);

        image =new Mat(photoCaptured.getHeight(),photoCaptured.getWidth(), CvType.CV_8UC3); //instantiation
        Utils.bitmapToMat(photoCaptured,image);


        GLES20.glGenTextures(1,textureHandle,0);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GL_TEXTURE_2D,textureHandle[0]);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D,0, photoCaptured, 0);

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);


        photoCaptured.recycle();
    }

    public void drawCube2(float[] mvpMatrix){
        GLES20.glUseProgram(mProgram);

        //**check if sqr or rect**
        if(image.cols() == image.rows() || Math.abs(image.cols()-image.rows())<20){
            Log.d("cube =", "is cube");
            Log.d("cube is cube",""+image.cols());
            Log.d("cube is cube",""+image.rows());
            Matrix.scaleM(mvpMatrix,0,2.0f,2.0f,2.0f);
        }else if(image.cols()> image.rows()){
            Log.d("cube =","is rectangle");
            Log.d("cube is rectangle cols",""+image.cols());
            Log.d("cube is rectangle rows",""+image.rows());
            Matrix.scaleM(mvpMatrix,0,3.0f,2.0f,2.0f);
        }else if(image.rows() > image.cols()){
            Matrix.scaleM(mvpMatrix,0,2.0f,3.0f,2.0f);
        }
        //** end **//

        //CUBE1

        vpositionHandle= GLES20.glGetAttribLocation(mProgram,"vPosition");

        GLES20.glEnableVertexAttribArray(vpositionHandle);
        GLES20.glVertexAttribPointer(vpositionHandle,COORDS_PER_VERTEX, GLES20.GL_FLOAT,false,vertex_stride,vertexBuffer);

        fcolorHandle= GLES20.glGetUniformLocation(mProgram,"v_color");
        GLES20.glUniform4fv(fcolorHandle,1,colors[0],0);
        textCoordHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        textUniformHandle = GLES20.glGetUniformLocation(mProgram, "s_Texture");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
        GLES20.glUniform1i(textUniformHandle, 0);

        textureBuffer.position(0);
        GLES20.glEnableVertexAttribArray(textCoordHandle);
        GLES20.glVertexAttribPointer(textCoordHandle,2, GLES20.GL_FLOAT,false,0,textureBuffer);

        Log.d("cube coord", ""+textCoordHandle);
        Log.d("cube uniform", ""+textUniformHandle);
        Log.d("cube image", ""+textureHandle[0]);

        mvpHandle= GLES20.glGetUniformLocation(mProgram,"uMVPmatrix");
        GLES20.glUniformMatrix4fv(mvpHandle,1,false,mvpMatrix,0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP,0,vertex_cnt);
        GLES20.glDisableVertexAttribArray(vpositionHandle);

    }

    private int cnt=0;
    public float[] translate(float[] currentmatrix,float x, float y, float z)
    {

        float[] tempModelMatrix = new float[16];
        Matrix.setIdentityM(tempModelMatrix, 0);
        Matrix.translateM(tempModelMatrix,0,x,y,z);
        for(float mtrx:tempModelMatrix){
            Log.d("Matrix value no."+cnt,""+mtrx);
        }
        Matrix.multiplyMM(currentmatrix, 0,
                currentmatrix, 0,tempModelMatrix, 0);
        return currentmatrix;
    }
    public float[] rotate(float[] currentmatrix,float angle, float x, float y, float z)
    {
        float[] tempModelMatrix = new float[16];
        Matrix.setIdentityM(tempModelMatrix, 0);
        Matrix.rotateM(tempModelMatrix,0,angle,x,y,z);
        for(float mtrx:tempModelMatrix){
            Log.d("ROTATION: Matrix value no."+cnt,""+mtrx);
        }
        Log.d("ROTATION: Matrix value no."+cnt,"------");
        Log.w("face"+cnt,""+x+","+y+","+z);
        Matrix.multiplyMM(currentmatrix, 0,
                currentmatrix, 0,tempModelMatrix, 0);
        cnt++;
        if(cnt==6){
            cnt=0;
        }
        return currentmatrix;
    }

}


