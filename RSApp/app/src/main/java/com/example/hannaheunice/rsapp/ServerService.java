package com.example.hannaheunice.rsapp;

/**
 * Created by john doe on 2/3/2017.
 */

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerService extends IntentService {

    private boolean serviceEnabled;

    private int port;
    private File saveLocation;
    private ResultReceiver serverResult;


    public static String origFilename;

    public ServerService() {
        super("ServerService");
        serviceEnabled = true;
    }

    @Override
    protected void onHandleIntent(Intent intent) {



        port = ((Integer) intent.getExtras().get("port")).intValue();
        saveLocation = (File) intent.getExtras().get("saveLocation");
        serverResult = (ResultReceiver) intent.getExtras().get("sR");
        Log.i("tost",saveLocation.toString());
        //signalActivity("Starting to download");


       // String fileName = "";

        ServerSocket welcomeSocket = null;
        Socket socket = null;

        try {


            welcomeSocket = new ServerSocket(port);

            while(true && serviceEnabled)
            {
                Log.i("tost",welcomeSocket.getInetAddress().toString());
                //Listen for incoming connections on specified port
                //Block thread until someone connects
                socket = welcomeSocket.accept();


                Log.i("tost","SERVER ACCEPTED");
                //signalActivity("TCP Connection Established: " + socket.toString() + " Starting file transfer");

                int bytesRead;


                InputStream is = socket.getInputStream();

                DataInputStream clientData = new DataInputStream(is);

                String fName = clientData.readUTF();
                Log.i("tost","Name of file: "+fName);
                origFilename=Environment.getExternalStorageDirectory().getPath() +"/Pictures/RushScan/" + fName;
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() +"/Pictures/RushScan/" + fName);
                long size = clientData.readLong();
                byte[] buffer = new byte[1024];
                while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
                {
                    output.write(buffer, 0, bytesRead);
                    size -= bytesRead;
                }

                // Closing the FileOutputStream handle
                is.close();
                clientData.close();
                output.close();



//                InputStreamReader isr = new InputStreamReader(is);
//                BufferedReader br = new BufferedReader(isr);
//
//                OutputStream os = socket.getOutputStream();
//                PrintWriter pw = new PrintWriter(os);
//
//
//                String inputData = "";
//
//
//
//               // signalActivity("About to start handshake");
//                //Client-Server handshake
//
//
//                //String savedAs = "WDFL_File_" + System.currentTimeMillis() + ".jpg";
//                String savedAs = "artoolkit.jpg";
//                File file = new File(saveLocation, savedAs);
//
//                byte[] buffer = new byte[4096];
//                int bytesRead;
//
//                FileOutputStream fos = new FileOutputStream(file);
//                BufferedOutputStream bos = new BufferedOutputStream(fos);
//
//                while(true)
//                {
//                    bytesRead = is.read(buffer, 0, buffer.length);
//                    if(bytesRead == -1)
//                    {
//                        break;
//                    }
//                    bos.write(buffer, 0, bytesRead);
//                    bos.flush();
//
//                }




            //    bos.close();
                socket.close();

                signalActivity(fName);
                //Start writing to file
             //   Log.i("tost","File Transfer Complete, saved as: " + savedAs);

            }


        } catch (IOException e) {
            Log.i("tost",e.getMessage());

        }
        catch(Exception e)
        {
            Log.i("tost",e.getMessage());

        }

        //Signal that operation is complete
        serverResult.send(port, null);

    }


    public void signalActivity(String message)
    {
        Bundle b = new Bundle();
        b.putString("message", message);
        serverResult.send(port, b);
    }


    public void onDestroy()
    {
        serviceEnabled = false;

        //Signal that the service was stopped
        //serverResult.send(port, new Bundle());

        stopSelf();
    }


}
