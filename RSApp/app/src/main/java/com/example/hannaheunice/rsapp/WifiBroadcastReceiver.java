package com.example.hannaheunice.rsapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by carizanatividad on 1/31/17.
 */

public class WifiBroadcastReceiver extends BroadcastReceiver {


    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private pulse activity;

    public WifiBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel,pulse activity) {
        super();
        this.manager = manager;
        this.channel = channel;
        this.activity = activity;

       // activity.setServerStatus("Server Broadcast Receiver created");

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);

            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
               Log.i("tost","Wifi Direct is enabled");
            } else {
                Log.i("tost","Wifi Direct is not enabled");
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // Call WifiP2pManager.requestPeers() to get a list of current peers
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            NetworkInfo networkState = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if(networkState.isConnected())
            {
                manager.requestConnectionInfo(channel,connectionInfoListener);
                Log.i("tost","Connection Status: Connected");
            }
            else
            {
                Log.i("tost","Connection Status: Disconnected");
                manager.cancelConnect(channel, null);

            }

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
        }
    }

    WifiP2pManager.ConnectionInfoListener connectionInfoListener=new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo info) {
//            InetAddress groupOwnerAddress=info.groupOwnerAddress;
            if(info.groupFormed){
                if(info.isGroupOwner){
                    Toast.makeText(activity, "Group owner",
                            Toast.LENGTH_SHORT).show();
                    manager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                        @Override
                        public void onGroupInfoAvailable(WifiP2pGroup group) {
                            Log.i("tost",group.toString());
                           //activity.setGroupInfo(group);
                        }
                    });
                }else{
                    Toast.makeText(activity, "client",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
