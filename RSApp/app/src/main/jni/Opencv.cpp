//
// Created by cariza natividad on 1/2/17.
//
#include "com_example_hannaheunice_rsapp_OpencvClass.h"
#include <string.h>
#include <math.h>
#include <algorithm>
#include <android/log.h>

JNIEXPORT jint JNICALL Java_com_example_hannaheunice_rsapp_OpencvClass_convertGray
        (JNIEnv *, jclass, jlong matBGR, jlong matGray){
    Mat& img=*(Mat*) matBGR;
    Mat& gray=*(Mat*) matGray;

    int conv;
    jint retval;
    conv=detectBlue(img,gray);
    retval=(jint)conv;
    return retval;
}

int toGray(Mat img, Mat& gray){
    cvtColor(img,gray,COLOR_RGB2GRAY);
    if(gray.rows==img.rows && gray.cols==img.cols)
        return 1;
     return 0;
}

double detectBlue(Mat& img,Mat& threshold_img){
    double distance=0;
    Mat imgclone;
    cvtColor(img,imgclone,COLOR_RGB2HSV);
    Scalar min(0,0,200,0);
    Scalar max(180,20,255,0);
    inRange(imgclone,min,max,threshold_img);

    threshold(threshold_img, threshold_img, 90, 255, THRESH_BINARY);
//    cvtColor(img,imgclone,COLOR_RGB2GRAY);
//    blur(imgclone,imgclone,Size(3,3));
//    Canny(imgclone,threshold_img,50,150,3);

    erode(threshold_img, threshold_img, Mat(), Point(-1,-1), 2,1,1);
//    dilate(threshold_img, threshold_img, 1, Point(-1,-1), 4,1,1);

    vector<vector<Size> > contours;
    vector<Vec4i> heirarchy;
    findContours(threshold_img, contours, heirarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));

    vector<Rect> boundRect(contours.size());
    vector<vector<Point> > contour_poly(contours.size());

    for(int i =0; i< contours.size(); i++)
    {
        approxPolyDP(Mat(contours[i]), contour_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contour_poly[i]));
    }
    int max_area=boundRect[0].area();
    int max_indx=0;
    for(int i=0;i<boundRect.size();i++){
        if(boundRect[i].area()>max_area){
            max_indx=i;
            max_area=boundRect[i].area();
        }
    }
    if(boundRect.size()>0){
        double num=boundRect[max_indx].area();
        string result;
        stringstream convert;
        convert << num;
        result=convert.str();
        rectangle( img, boundRect[max_indx].tl(), boundRect[max_indx].br(),Scalar(0,255,0,0), 2, 8, 0 );
        putText(img, result,boundRect[max_indx].tl(),FONT_HERSHEY_SCRIPT_SIMPLEX,2,Scalar(255,0,0,0),3,8,false);
        distance=8414.7*pow(boundRect[max_indx].area(), -0.468);
        stringstream conv;
        string dist;
        conv << distance;
        dist=conv.str();
        putText(img, "dist="+dist,boundRect[max_indx].br(),FONT_HERSHEY_SCRIPT_SIMPLEX,2,Scalar(255,0,0,0),2,8,false);
        cout << distance << " cm." ;
    }
    return distance;
}

JNIEXPORT void JNICALL Java_com_example_hannaheunice_rsapp_OpencvClass_docuScanner
        (JNIEnv *env, jclass, jstring filePath){
    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);

    __android_log_write(ANDROID_LOG_INFO,"contour filepath",nativeString);
    Mat image;

    image=imread(nativeString,1);

    __android_log_write(ANDROID_LOG_INFO,"contour filepath","image");
    Mat img_clone=image.clone();
    __android_log_write(ANDROID_LOG_INFO,"contour filepath","imgclone");

    double ratio= image.rows/500.0;

    string resultca;
    stringstream convertca;
    convertca << image.rows;
    convertca << " x ";
    convertca << image.cols;
    resultca=convertca.str();
    char const* pachaa = resultca.c_str();
    __android_log_write(ANDROID_LOG_INFO, "resizr=",pachaa);

    resize(image,image,Size(image.cols/ratio,500));
    __android_log_write(ANDROID_LOG_INFO,"contour filepath","resize");

    Mat img_gray;
    Mat img_canny;
    cvtColor(image,img_gray,COLOR_RGB2GRAY);
    __android_log_write(ANDROID_LOG_INFO,"contour filepath","rgb2gray");
    GaussianBlur(img_gray,img_gray,Size(5,5),0);
    Canny(img_gray,img_canny,75,200,3);

    vector<vector<Point> > contours;
    vector<Vec4i> heirarchy;
    findContours(img_canny, contours, heirarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
    //**DEBUG CONTOUR AREA**//
    for(int i=0;i<contours.size();i++){
        double area=contourArea(contours[i]);
        string resultc;
        stringstream convertc;
        convertc << area;
        resultc=convertc.str();
        char const* pchaa = resultc.c_str();
        __android_log_write(ANDROID_LOG_INFO, "contour area docuscanner=",pchaa);
    }
    //**END DEBUG UNSORTED CONTOUR AREA**//
    sort(contours.begin(), contours.end(), compareContourAreas);
    //**DEBUG SORTED CONTOUR AREA**//
    for(int i=0;i<contours.size();i++){
        double area=contourArea(contours[i]);
        string resultc;
        stringstream convertc;
        convertc << area;
        resultc=convertc.str();
        char const* pchaa = resultc.c_str();
        __android_log_write(ANDROID_LOG_INFO, "contour area sorted=",pchaa);
    }
    //**END DEBUG UNSORTED CONTOUR AREA**//

    double perimeter;
    vector<Point> approx;
    vector<vector<Point> >  screencnt;
    int pos=-1;
    int contLen=contours.size();
    if(contLen>5){
        contLen=5;
        //** LESS CONTOUR SIZE, SCANNING FAILED
        for(int i=0;i<contLen;i++){
            perimeter=arcLength(contours[i],true);
            approxPolyDP(contours[i],approx,(0.02*perimeter),true);
            if(approx.size()==4 && fabs(contourArea(approx)) > 1000 && isContourConvex(approx)){
                int vtc = approx.size();
                // Get the cosines of all corners
                double cos;
                double maxCos=0;
                for (int j = 2; j < vtc+1; j++){
                    cos=fabs(angle2(approx[j%vtc], approx[j-2], approx[j-1]));
                    maxCos=MAX(maxCos,cos);
                }
                string maxcostr;
                stringstream maxcosstring;
                maxcosstring << maxCos;
                maxcostr=maxcosstring.str();
                char const* pchamax = maxcostr.c_str();
                __android_log_write(ANDROID_LOG_INFO, "contour area maxcos=",pchamax);

                if(vtc==4 && maxCos <0.3){
//                approx=approx1;
                    pos=i;
                    i=contours.size();

                    //** DEBUG approx **//
                    string resultam;
                    stringstream convertam;
                    convertam << approx;
                    resultam=convertam.str();
                    char const* pchaam = resultam.c_str();
                    __android_log_write(ANDROID_LOG_INFO, "contour area approx=",pchaam);
                    //**END DEBUG approx**//
                }
            }
        }
        __android_log_write(ANDROID_LOG_INFO, "approx","ok");

        if(pos!=-1) {
            drawContours(image, contours, pos, Scalar(255, 0, 0, 255), 2);
            imwrite(nativeString,image);
            //** DEBUG**//
            for (int i = 0; i < approx.size(); i++) {
                approx[i]=approx[i]*ratio;
                string resultc;
                stringstream convertc;
                convertc << approx[i];
                resultc = convertc.str();
                char const *pchaa = resultc.c_str();
                __android_log_write(ANDROID_LOG_INFO, "app=", pchaa);
            }
            //**DEBUG**//

            //CONVERT POINTS TO ARRAY
            int last = approx.size();
            double rectPoints[last][2];
            for (int i = 0; i < last; i++) {
                rectPoints[i][0] = approx[i].x;
                rectPoints[i][1] = approx[i].y;
            }
            //END OF CONVERSION

            //** ORDERING POINTS **//
            //sum axis 1
            double sum[last];
            double s;
            for (int i = 0; i < last; i++) {
                for (int j = 0; j < 2; j++) {
                    s = s + rectPoints[i][j];
                }
                sum[i] = s;
                s = 0;
                string resultc;
                stringstream convertc;
                convertc << sum[i];
                resultc = convertc.str();
                char const *pchaa = resultc.c_str();
                __android_log_write(ANDROID_LOG_INFO, "app sum=", pchaa);
            }
            vector<double> sumx(sum, sum + last);
            sort(sumx.begin(), sumx.end(), myfunction);
            double tlval = sumx[0];
            double brval = sumx[last - 1];
            int tlpos;
            int brpos;
            for (int i = 0; i < last; i++) {
                for (int j = 0; j < 2; j++) {
                    s = s + rectPoints[i][j];
                }
                if (s == tlval) {
                    tlpos = i;
                }
                if (s == brval) {
                    brpos = i;
                }
                s = 0;
            }
            Point tl = approx[tlpos];
            Point br = approx[brpos];

            //**DEBUG**//
            string tlc;
            stringstream converttl;
            converttl << tl;
            tlc = converttl.str();
            char const *tll = tlc.c_str();
            __android_log_write(ANDROID_LOG_INFO, "app tl=", tll);

            string brc;
            stringstream convertbr;
            convertbr << br;
            brc = convertbr.str();
            char const *brr = brc.c_str();
            __android_log_write(ANDROID_LOG_INFO, "app br=", brr);

            //**END DEBUG**//

            //diff axis=0
            double diff[last];
            int d = 0;
            for (int i = 0; i < last; i++) {
                for (int j = 0; j < 2; j++) {
                    d = rectPoints[i][j] - d;
                }
                diff[i] = d;
                d = 0;
                string resultc;
                stringstream convertc;
                convertc << diff[i];
                resultc = convertc.str();
                char const *pchaa = resultc.c_str();
                __android_log_write(ANDROID_LOG_INFO, "app diff=", pchaa);
            }
            vector<double> diffx(diff, diff + last);
            sort(diffx.begin(), diffx.end(), myfunction);
            double trval = diffx[0];
            double blval = diffx[last - 1];
            int trpos = -1, blpos = -1;
            int posused;
            for (int i = 0; i < last; i++) {
                if (i != tlpos && i != brpos) {
                    for (int j = 0; j < 2; j++) {
                        d = rectPoints[i][j] - d;
                    }
                    if (d == trval) {
                        trpos = i;
                        posused = i;
                    }
                    if (d == blval) {
                        blpos = i;
                        posused = i;
                    }
                    d = 0;
                }
            }
            if (trpos == -1 || blpos == -1) {
                for (int i = 0; i < last; i++) {
                    if (i != tlpos && i != brpos && i != posused) {
                        if (trpos == -1) {
                            trpos = i;
                        } else {
                            blpos = i;
                        }
                    }
                }
            }
            Point tr = approx[trpos];
            Point bl = approx[blpos];

            //**DEBUG**//
            string trl;
            stringstream converttr;
            converttr << tr;
            trl = converttr.str();
            char const *tlrr = trl.c_str();
            __android_log_write(ANDROID_LOG_INFO, "app tr=", tlrr);

            string blc;
            stringstream convertbl;
            convertbl << bl;
            blc = convertbl.str();
            char const *brl = blc.c_str();
            __android_log_write(ANDROID_LOG_INFO, "app bl=", brl);

            //**END DEBUG**//

            //**END ORDERING POINTS**//

            //**PERSPECTIVE AND WARP**//
            Point2f input[4];
            input[0] = tl;
            input[1] = tr;
            input[2] = br;
            input[3] = bl;
            Mat inputMat = Mat(4, 2, CV_32F, input);

            double widthA = sqrt(pow((br.x - bl.x), 2) + pow((br.y - bl.y), 2));
            double widthB = sqrt(pow((tr.x - tl.x), 2) + pow((tr.y - tl.y), 2));
            double maxWidth = max(widthA, widthB);

            double heightA = sqrt(pow((tr.x - br.x), 2) + pow((tr.y - br.y), 2));
            double heightB = sqrt(pow((tl.x - bl.x), 2) + pow((tl.y - bl.y), 2));
            double maxHeight = max(heightA, heightB);

            Point2f dst[4];
            dst[0] = Point2f(0, 0);
            dst[1] = Point2f(maxWidth - 1, 0);
            dst[2] = Point2f(maxWidth - 1, maxHeight - 1);
            dst[3] = Point2f(0, maxHeight - 1);
            Mat dstMat;

            Mat M = getPerspectiveTransform(input, dst);
            warpPerspective(img_clone, dstMat, M, Size(maxWidth, maxHeight));
            // END//
//
   //         cvtColor(dstMat, dstMat, COLOR_RGB2GRAY);
   //         adaptiveThreshold(dstMat, dstMat, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 251,10);


            //const char* s1 = ".png";
           // strcat(const_cast<char*>(nativeString),s1);
            //    Mat& img=*(Mat*) matBGR;

//         removeWhite(dstMat);
            imwrite(nativeString, dstMat);
            //**END POINTS**//
        }
    }

}

JNIEXPORT void JNICALL Java_com_example_hannaheunice_rsapp_OpencvClass_scanCrop
(JNIEnv *env, jclass, jstring filePath){

    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);

    __android_log_write(ANDROID_LOG_INFO,"crop filepath",nativeString);
    Mat image;
    Mat img;
    Mat img_gray;
    Mat img_can;


    image=imread(nativeString,1);
    __android_log_write(ANDROID_LOG_INFO,"crop ","loaded");

    while(image.empty()){
        __android_log_write(ANDROID_LOG_INFO,"crop ","empty");
        __android_log_write(ANDROID_LOG_INFO,"crop filepath",nativeString);
        image=imread(nativeString,1);
    }

    if(image.type()!=CV_8UC1){
        cvtColor(image, img_gray, COLOR_RGBA2GRAY);
        __android_log_write(ANDROID_LOG_INFO,"crop ","color");
        adaptiveThreshold(img_gray, img_can, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 251,10);
        __android_log_write(ANDROID_LOG_INFO,"crop ","thresh");
    }else{
    adaptiveThreshold(image, img_can, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 251,10);
    __android_log_write(ANDROID_LOG_INFO,"crop ","thresh");
    }

    //    removeWhite(image);

    //**DEBUG**//
    string trl;
    stringstream converttr;
    converttr << "ROWS: ";
    converttr << img_can.rows;
    converttr << " COLS: ";
    converttr << img_can.cols;
    trl = converttr.str();
    char const *tlrr = trl.c_str();
    __android_log_write(ANDROID_LOG_INFO, "tost", tlrr);

    imwrite(nativeString, img_can);
}
JNIEXPORT void JNICALL Java_com_example_hannaheunice_rsapp_OpencvClass_removeWhite
        (JNIEnv *env, jclass, jstring filePath){
    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);

    __android_log_write(ANDROID_LOG_INFO,"TOGGLE png filepath",nativeString);
    Mat image;

    image=imread(nativeString,1);
    __android_log_write(ANDROID_LOG_INFO,"TOGGLE png read","image");
    cvtColor(image, image, COLOR_BGR2GRAY);
    //DEBUG
    if(image.type()!=CV_8UC1){
        __android_log_write(ANDROID_LOG_INFO,"TOGGLE png ","NOT ONE CHANNELED");
    }else{
        __android_log_write(ANDROID_LOG_INFO,"TOGGLE png ","GRAYED");
    }
    //END
    removeWhite(image);
    __android_log_write(ANDROID_LOG_INFO,"TOGGLE png  ","REMOVED");
    imwrite(nativeString, image);
}

void removeWhite(Mat& input){
    Mat input_bgra;
    cvtColor(input, input_bgra, CV_GRAY2BGRA);

    // find all white pixel and set alpha value to zero:
    for (int y = 0; y < input_bgra.rows; ++y)
        for (int x = 0; x < input_bgra.cols; ++x)
        {
            cv::Vec4b & pixel = input_bgra.at<cv::Vec4b>(y, x);
            // if pixel is white
            if (pixel[0] == 255 && pixel[1] == 255 && pixel[2] == 255)
            {
                // set alpha to zero:
                pixel[3] = 0;
//                __android_log_write(ANDROID_LOG_INFO,"TOGGLE png  ","REMOVED");
            }
        }
    input=input_bgra;
}
JNIEXPORT void JNICALL Java_com_example_hannaheunice_rsapp_OpencvClass_addWhite
        (JNIEnv *env, jclass, jstring filePath){
    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);

    __android_log_write(ANDROID_LOG_INFO,"TOGGLE png filepath",nativeString);
    Mat image;
    image=imread(nativeString,1);
    cvtColor(image, image, COLOR_BGR2GRAY);
    if(image.type()!=CV_8UC1){
        __android_log_write(ANDROID_LOG_INFO,"TOGGLE png add ","NOT ONE CHANNELED");
    }else{
        __android_log_write(ANDROID_LOG_INFO,"TOGGLE png add ","GRAYED");
    }
    addWhite(image);
    imwrite(nativeString, image);
}
void addWhite(Mat& input){
    Mat input_bgra;
    cvtColor(input, input_bgra, CV_GRAY2BGRA);

    // find all white pixel and set alpha value to zero:
    for (int y = 0; y < input_bgra.rows; ++y)
        for (int x = 0; x < input_bgra.cols; ++x)
        {
            cv::Vec4b & pixel = input_bgra.at<cv::Vec4b>(y, x);
            // if pixel is white
            if (pixel[3] == 0)
            {
                // set alpha to zero:
//                pixel[3] = 0;
                pixel[0] = 255;
                pixel[1] = 255;
                pixel[2] = 255;
            }
        }
    input=input_bgra;
}

bool compareContourAreas ( std::vector<cv::Point> contour1, std::vector<cv::Point> contour2 ) {
    double i = fabs( contourArea(cv::Mat(contour1)) );
    double j = fabs( contourArea(cv::Mat(contour2)) );
    return ( i > j );
}
bool myfunction (int i,int j) {
    return (i<j);
}
static double angle2(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}
