LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#opencv
OPENCVROOT:= C:/OpenCV-android-sdk
OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED
include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk

LOCAL_SRC_FILES:= Opencv.cpp
LOCAL_SRC_FILES+=CalibrateCamera.cpp

LOCAL_LDLIBS+= -llog
LOCAL_MODULE:= MyOpenCVLibs

include $(BUILD_SHARED_LIBRARY)