//
// Created by cariza natividad on 1/11/17.
//
#include "com_example_hannaheunice_rsapp_CalibrateCamera.h"
#include <android/log.h>
#include <string.h>

JNIEXPORT jboolean JNICALL Java_com_example_hannaheunice_rsapp_CalibrateCamera_canCalibrate
        (JNIEnv * env, jclass, jlong frameAddr, jstring filePath){

    Mat& frame=*(Mat*) frameAddr;
    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);
    // use your string
//    (*env). ReleaseStringUTFChars(filePath,nativeString);
    __android_log_write(ANDROID_LOG_INFO, "calib PATH",nativeString);
    char* path=(char*) nativeString;

    Mat frame_gray;
    std::vector<Point2f> corners;

    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    jboolean canCalib=findChessboardCorners(frame_gray,Size(7,7),corners,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
    int calib_count=0;

    return canCalib;
}

JNIEXPORT jobjectArray JNICALL Java_com_example_hannaheunice_rsapp_CalibrateCamera_getImagePoints
        (JNIEnv * env, jclass, jlong frameAddr, jobjectArray savedImgpts, int snap){
    Mat& framee=*(Mat*)frameAddr;
    Mat frame=(Mat)framee;
    Mat frame_gray;

    int board_size=49;
    std::vector<Point2f> corners;
    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    vector<vector<Point2f> > imagePoints;

    jboolean canCalib=findChessboardCorners(frame_gray,Size(7,7),corners,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
    cornerSubPix( frame_gray, corners, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

    //***DEBUG CORNERS**//
    for(int i = 0; i < corners.size(); i++) {
//            newViewMat[x]= transposeViewMat.at<double> (i,j);
            string resultr;
            stringstream convertr;
            convertr << corners[i].x;
            resultr=convertr.str();
            char const* pcharr = resultr.c_str();
            __android_log_write(ANDROID_LOG_INFO, "IMAGEPOINTS x=",pcharr);

        string resultc;
        stringstream convertc;
        convertc << corners[i].y;
        resultc=convertc.str();
        char const* pchaa = resultc.c_str();
        __android_log_write(ANDROID_LOG_INFO, "IMAGEPOINTS y=",pchaa);

    }

    //**END CORNERS DEBUG**//
    imagePoints.push_back(corners); //imagepoints

    //**DEBUG**//
    __android_log_write(ANDROID_LOG_INFO, "IMAGEPOINTS ","push bk");
    string sizeimg;
    stringstream convertx;
    convertx << imagePoints[0].size()*2;
    sizeimg=convertx.str();
    char const* pchax = sizeimg.c_str();
    __android_log_write(ANDROID_LOG_INFO, "IMAGEPOINTS size=",pchax);
    jclass floatArrayClass = (*env). FindClass("[F");
    //**END DEBUG**//

    //OBJECTARRAY TO RETURN
    jobjectArray imgpts= (*env). NewObjectArray(snap+1,floatArrayClass,0);
    if(snap!=0){
        for(int i = 0; i < snap; i++) {
            jfloatArray row=(jfloatArray) (*env).GetObjectArrayElement(savedImgpts,i);
            (*env). SetObjectArrayElement(imgpts,i,row);
            (*env). DeleteLocalRef(row);
        }
    }
        jfloatArray floatArray = (*env). NewFloatArray(imagePoints[0].size()*2);
        (*env). SetFloatArrayRegion(floatArray,0, imagePoints[0].size()*2,(jfloat*) imagePoints[0].data());
        (*env). SetObjectArrayElement(imgpts,snap,floatArray);
        (*env). DeleteLocalRef(floatArray);
    return imgpts;
}
JNIEXPORT jobjectArray JNICALL Java_com_example_hannaheunice_rsapp_CalibrateCamera_getObjectPoints
        (JNIEnv * env, jclass, jlong frameAddr, jobjectArray savedObjPts, jint snap){
    Mat& framee=*(Mat*)frameAddr;
    Mat frame=(Mat)framee;
    Mat frame_gray;
    vector<vector<Point3f> > objectPoints;
    vector<Point3f> objectCorners;
    for(int i=0;i<49;i++){
        objectCorners.push_back(Point3f((float) (i/7),(float) (i%7),0.0f)); //why dis values; 3D MODEL POINTS
    }
    objectPoints.push_back(objectCorners);

    //**DEBG**//
    string sizeimg;
    stringstream convertx;
    convertx << objectPoints[0].size();
    sizeimg=convertx.str();
    char const* pchax = sizeimg.c_str();
    __android_log_write(ANDROID_LOG_INFO, "IMAGEPOINTS size=",pchax);
    //**DEBUG**//

    jclass floatArrayClass = (*env). FindClass("[F");

    jobjectArray objPts=(*env). NewObjectArray(snap+1,floatArrayClass,0);
    if(snap!=0){
        for(int i = 0; i < snap; i++) {
            jfloatArray row=(jfloatArray) (*env).GetObjectArrayElement(savedObjPts,i);
            (*env). SetObjectArrayElement(objPts,i,row);
            (*env). DeleteLocalRef(row);
        }
    }
    jfloatArray floatArray = (*env). NewFloatArray(objectPoints[0].size()*3);
    (*env). SetFloatArrayRegion(floatArray,0, objectPoints[0].size()*3,(jfloat*) objectPoints[0].data());
    (*env). SetObjectArrayElement(objPts,snap,floatArray);
    (*env). DeleteLocalRef(floatArray);
    return objPts;
}

JNIEXPORT void JNICALL Java_com_example_hannaheunice_rsapp_CalibrateCamera_calibrateSave
        (JNIEnv * env, jclass, jlong frameAddr, jstring filePath, jobjectArray savedImagePoints, jobjectArray savedObjectPoints){
    Mat& framee=*(Mat*)frameAddr;
    Mat frame=(Mat)framee;
    Mat frame_gray;

    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);
    __android_log_write(ANDROID_LOG_INFO, "calib PATH save",nativeString);

    //**GET IMAGE POINTS**//
    vector<vector<Point2f> > final_imagepts;
    vector<Point2f> imagePts;
    int rowlen=(*env).GetArrayLength(savedImagePoints);
    for(int i=0;i<rowlen;i++){
        jfloatArray row=(jfloatArray) (*env).GetObjectArrayElement(savedImagePoints,i);
        jfloat *elemnt=(*env).GetFloatArrayElements(row,0);
        int collen=(*env).GetArrayLength(row);
        for(int j=0;j<collen-1;j+=2){
            imagePts.push_back(Point2f(elemnt[j],elemnt[j+1]));
            string result;
            stringstream convert;
            convert << elemnt[j];
            result=convert.str();
            char const* pchar = result.c_str();
            __android_log_write(ANDROID_LOG_INFO, "element",pchar);
        }
        (*env).DeleteLocalRef(row);
    }
    final_imagepts.push_back(imagePts);
    //**END IMAGE POINTS**//

    //**GET OBJECT POINTS**//
    vector<vector<Point3f> > final_objpts;
    vector<Point3f> objPts;
    rowlen=(*env).GetArrayLength(savedObjectPoints);
    for(int i=0;i<rowlen;i++){
        jfloatArray row=(jfloatArray) (*env).GetObjectArrayElement(savedObjectPoints,i);
        jfloat *elemnt=(*env).GetFloatArrayElements(row,0);
        int collen=(*env).GetArrayLength(row);
        for(int j=0;j<collen-2;j+=3){
            objPts.push_back(Point3f(elemnt[j],elemnt[j+1],elemnt[j+2]));
            string result;
            stringstream convert;
            convert << elemnt[j];
            result=convert.str();
            char const* pchar = result.c_str();
            __android_log_write(ANDROID_LOG_INFO, "element",pchar);
        }
        (*env).DeleteLocalRef(row);
    }
    final_objpts.push_back(objPts);
    //**END OBJECT POINTS**//
    //**DEBUG**//
    string resultf;
    stringstream convertf;
    convertf << final_imagepts[0].size();
    resultf=convertf.str();
    char const* pcharf = resultf.c_str();
    __android_log_write(ANDROID_LOG_INFO, "final imagepts size= ",pcharf);

    string resultv;
    stringstream convertv;
    convertv << final_objpts[0].size();
    resultv=convertv.str();
    char const* pcharv = resultv.c_str();
    __android_log_write(ANDROID_LOG_INFO, "final objpts size= ",pcharv);

    //**END DEBUG**//

    //**CALIBRATE CAMERA AND SAVE DATA**//
//    std::vector<Point2f> corners;
////
//    cvtColor( frame, frame_gray, CV_BGR2GRAY );
//    jboolean canCalib=findChessboardCorners(frame_gray,Size(7,7),corners,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
//    int calib_count=0;
//    if(canCalib){
//        if(corners.size()==49){ //should be equal to bordersize
            vector<vector<Point2f> > imagePoints;
            vector<vector<Point3f> >objectPoints;
            vector<Point3f> objectCorners;
            Mat camera_matrix  = Mat::eye(3,3,CV_64F); //intrinsic
            Mat distortion_coeffs = Mat::zeros(5,1,CV_64F); //4,5,8
            vector<Mat> rvecs,tvecs;
//
            camera_matrix.at<double> (0,0)=1.0;
//            cornerSubPix( frame_gray, corners, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
//            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","bbb");
//            drawChessboardCorners(frame,Size(7,7),corners,canCalib);
//
//            imagePoints.push_back(corners); //imagepoints
//            for(int i=0;i<49;i++){
//                objectCorners.push_back(Point3f((float) (i/7),(float) (i%7),0.0f)); //why dis values; 3D MODEL POINTS
//            }
//            objectPoints.push_back(objectCorners);
//            calib_count++;
//            string result;
//            stringstream convert;
//            convert << calib_count;
//            result=convert.str();
//            char const* pchar = result.c_str();
//            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count",pchar);
//
            calibrateCamera(final_objpts,final_imagepts,frame.size(),camera_matrix,distortion_coeffs,
                            rvecs,tvecs,CV_CALIB_FIX_K4|CV_CALIB_FIX_K5,TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
//            //***SAVE CALIBRATED CAMERA DATA**//
            char* path=(char*) nativeString;
            char path_for_intrinsic[strlen(path)+100];
            strcpy(path_for_intrinsic,path);
            __android_log_write(ANDROID_LOG_INFO, "Can calibrate","path ok!");
//
            FileStorage fs(strcat(path_for_intrinsic,"/xml/sample_calibrate_intrinsic.xml"),FileStorage::WRITE); //FIND WAY TO CONTATENATE
            fs << "intrinsic" << camera_matrix;
            fs.release();
            __android_log_write(ANDROID_LOG_INFO, "calib PATH save intrinsic",path_for_intrinsic);

            char path_for_distortion[strlen(path)+100];
            strcpy(path_for_distortion,path);
            FileStorage fs2(strcat(path_for_distortion,"/xml/sample_calibrate_distortion.xml"),FileStorage::WRITE);
            fs2 << "distortion_coefficients" << distortion_coeffs;
            fs2.release();
            __android_log_write(ANDROID_LOG_INFO, "calib PATH save distortion",path_for_distortion);
//        }
//    }
    //**END CAMERA CALIBRATION AND SAVING**//
}


// AUGMENT 3D
JNIEXPORT jfloatArray JNICALL Java_com_example_hannaheunice_rsapp_CalibrateCamera_augment
        (JNIEnv * env, jclass, jlong frameAddr, jstring filePath){
    //POSE ESTIMATION

    Mat camera_matrix=Mat(3,3,CV_64F); //intrinsic
    Mat distortion_coeff=Mat(5,1,CV_64F);

    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);
    __android_log_write(ANDROID_LOG_INFO, "calib PATH",nativeString);

    char* path=(char*) nativeString;
    char path_for_intrinsic[strlen(path)+100];
    strcpy(path_for_intrinsic,path);

    FileStorage fsIntrinsic;
    fsIntrinsic.open(strcat(path_for_intrinsic,"/xml/sample_calibrate_intrinsic.xml"), FileStorage::READ);
    __android_log_write(ANDROID_LOG_INFO, "Can calibrate read intrinsic",path_for_intrinsic);

    if(!fsIntrinsic.isOpened()){
        __android_log_write(ANDROID_LOG_INFO, "calibrate read intrinsic","File cant open!");
    }else{
        __android_log_write(ANDROID_LOG_INFO, "calibrate read intrinsic","File  openED!");

        fsIntrinsic["intrinsic"] >> camera_matrix;
        string resultr;
            stringstream convertr;
            convertr << camera_matrix.at<double> (0,0);
            resultr=convertr.str();
            char const* pcharr = resultr.c_str();
            __android_log_write(ANDROID_LOG_INFO, "Can calibrate read intrinsic",pcharr);

    }
    fsIntrinsic.release();
    char path_for_distortion[strlen(path)+100];
    strcpy(path_for_distortion,path);

    FileStorage fsDistortion;
    fsDistortion.open(strcat(path_for_distortion,"/xml/sample_calibrate_distortion.xml"), FileStorage::READ);
    __android_log_write(ANDROID_LOG_INFO, "Can calibrate read distortion",path_for_distortion);
    if(!fsDistortion.isOpened()){
        __android_log_write(ANDROID_LOG_INFO, "calibrate read distortion","File cant open!");
    }else{
        __android_log_write(ANDROID_LOG_INFO, "calibrate read distortion","File openED!");
        fsDistortion["distortion_coefficients"] >> distortion_coeff;
        string resultd;
        stringstream convertd;
        convertd << distortion_coeff.at<double> (0,0);
        resultd=convertd.str();
        char const* pchard = resultd.c_str();
        __android_log_write(ANDROID_LOG_INFO, "Can calibrate read distortion",pchard);
    }

    fsDistortion.release();

    __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","fileloaded");


//    vector<vector<Point3f> >objectPoints;
//    std::vector<cv::Vec3f> model_points; //3D
//    model_points.push_back(cv::Point3f(0.0f, 0.0f, 0.0f));               // Nose tip
//    model_points.push_back(cv::Point3f(1.0f, 0.0f, 0.0f));          // Chin
//    model_points.push_back(cv::Point3f(1.0f, 1.0f, 0.0f));       // Left eye left corner
//    model_points.push_back(cv::Point3f(0.0f, 1.0f, 0.0f));        // Right eye right corner

    std::vector<cv::Point3d> objectPoints;
    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) {
            objectPoints.push_back(
                    cv::Point3d(double(j * 7), float(i * 7), 0));
        }
    }
//    objectPoints.push_back(cv::Point3d(-1.0, -1.0f, 0));
//    objectPoints.push_back(cv::Point3d(1.0, -1.0f, 0));
//    objectPoints.push_back(cv::Point3d(-1.0,  1.0f, 0));
//    objectPoints.push_back(cv::Point3d(1.0,  1.0f, 0));


//    objectPoints.push_back(model_points);
    vector<vector<Point2f> > imagePoints;
    Mat& framee=*(Mat*)frameAddr;
    Mat frame=(Mat)framee;
    Mat frame_gray;
    std::vector<Point2f> corners;

    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    jboolean canCalib=findChessboardCorners(frame_gray,Size(7,7),corners,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
    jfloatArray vM;

//    if(canCalib){
        Mat rvecs,tvecs;
        cornerSubPix( frame_gray, corners, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.01 ));
        __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","cornerSubPix success!");

//        imagePoints.push_back(corners);
        solvePnP(objectPoints,corners,camera_matrix,distortion_coeff,rvecs,tvecs,false,1);
        __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","solvePnP success!");

        vector<Point3f> axis;
        std::vector<Point2f> imagepts;
        axis.push_back(Point3f(0,0,1));
        __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","axis");

//
//        projectPoints(axis,rvecs,tvecs,camera_matrix,distortion_coeff,imagepts); //NOT NEEDED
//        __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","projectPoints success");

        if(rvecs.rows > 0 && tvecs.cols > 0){
            Mat rotation_mat=Mat(3,3,CV_64F);;
            Rodrigues(rvecs,rotation_mat);
            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","rodrigues success");

//            vector<Point4d> viewMat;
            Mat viewMat  = Mat(4,4,CV_64F);
            for(int i = 0; i < viewMat.rows; i++) {
                for (int j = 0; j < viewMat.cols; j++) {
                    if(i==3){
                        if(j<3){
                            viewMat.at<double> (i,j) =0.0f;
                        }else{
                            viewMat.at<double> (i,j) =1.0f;
                        }
                    }else{
                        if(j<3){
                            viewMat.at<double> (i,j) = rotation_mat.at<double> (i,j);
                        }else{
                            viewMat.at<double> (i,j) = tvecs.at<double> (i,0);
                        }
                    }
                }
            }

            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","viewMat ok");
//
//            Mat inverseMat  = Mat(4,4,CV_64F);
//            for(int i = 0; i < inverseMat.rows; i++) {
//                for (int j = 0; j < inverseMat.cols; j++) {
//                    if(i==0 || i==3){
//                        inverseMat.at<double> (i,j) = 1.0f;
//                    }else{
//                        inverseMat.at<double> (i,j) = -1.0f;
//                    }
//                }
//            }
//////
//            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","inverseMat ok");
////
            cv::Mat cvToGl = cv::Mat::zeros(4, 4, CV_64F);
            cvToGl.at<double>(0, 0) = 1.0f;
            cvToGl.at<double>(1, 1) = -1.0f;// Invert the y axis
            cvToGl.at<double>(2, 2) = -1.0f; // invert the z axis
            cvToGl.at<double>(3, 3) = 1.0f;
            viewMat = cvToGl * viewMat;
////            viewMat=viewMat*inverseMat;
//            Mat mult  = Mat(4,4,CV_64F);
//            for(int i = 0; i < inverseMat.rows; i++) {
//                for (int j = 0; j < inverseMat.cols; j++) {
//                    for(int k=0; k<inverseMat.rows; k++)
//                    {
//                        mult.at<double> (i,j) += inverseMat.at<double> (i,k) * viewMat.at<double> (k,j);
//                    }
//                }
//            }
//            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","multiply ok");

//
            Mat transposeViewMat=Mat(4,4,CV_64F);
            for(int i = 0; i < viewMat.rows; i++) {
                for (int j = 0; j < viewMat.cols; j++) {
                    transposeViewMat.at<double> (j,i) = viewMat.at<double> (i,j);
                }
            }

            //convert Mat to float[]
            jfloat newViewMat[16];
            int x=0;
            for(int i = 0; i < viewMat.rows; i++) {
                for (int j = 0; j < viewMat.cols; j++) {
                    newViewMat[x]= transposeViewMat.at<double> (i,j);

                    string resultr;
                    stringstream convertr;
                    convertr << newViewMat[x];
                    resultr=convertr.str();
                    char const* pcharr = resultr.c_str();
                    __android_log_write(ANDROID_LOG_INFO, "Can calibrate VIEMAT CPP",pcharr);

                    x++;
                }
            }
            //DISPLAY debug
            for(int i = 0; i < 16; i++) {

                    string resultr;
                    stringstream convertr;
                    convertr << newViewMat[i];
                    resultr=convertr.str();
                    char const* pcharr = resultr.c_str();
                    __android_log_write(ANDROID_LOG_INFO, "Can calibrate VIEMAT CPP",pcharr);

            }
            jfloatArray vM;
            vM = (*env). NewFloatArray(16);
            (*env). SetFloatArrayRegion(vM,0,16,newViewMat);

            __android_log_write(ANDROID_LOG_INFO, "Can calibrate count","vM ok");

            return vM;
//            // return transposeViewMat
        }
//    }
    return vM;
}

//AUGMENT USING CONTOURS
JNIEXPORT jfloatArray JNICALL Java_com_example_hannaheunice_rsapp_CalibrateCamera_augmentUsingContours
        (JNIEnv *env, jclass, jlong frameAddr, jstring filePath){

__android_log_write(ANDROID_LOG_INFO, "augment ","augment start");
//    const char *nativeString = (*env). GetStringUTFChars(filePath, 0);
__android_log_write(ANDROID_LOG_INFO, "augment ","augment start");

Mat& image=*(Mat*) frameAddr;
__android_log_write(ANDROID_LOG_INFO, "augment ","augment frame");
Mat img_gray;
Mat img_canny;
cvtColor(image,img_gray,COLOR_RGB2GRAY);
__android_log_write(ANDROID_LOG_INFO, "augment ","augment gray");

GaussianBlur(img_gray,img_gray,Size(5,5),0);
__android_log_write(ANDROID_LOG_INFO, "augment ","augment gaussian");

Canny(img_gray,img_canny,100,200,3);
__android_log_write(ANDROID_LOG_INFO, "augment ","augment canny");

vector<vector<Point> > contours;
vector<Vec4i> heirarchy;
findContours(img_canny, contours, heirarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
//**DEBUG CONTOUR AREA**//
for(int i=0;i<contours.size();i++){
double area=contourArea(contours[i]);
string resultc;
stringstream convertc;
convertc << area;
resultc=convertc.str();
char const* pchaa = resultc.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment contour area=",pchaa);
}
//**END DEBUG UNSORTED CONTOUR AREA**//
sort(contours.begin(), contours.end(), compareContourAreas2);
//**DEBUG SORTED CONTOUR AREA**//
for(int i=0;i<contours.size();i++){
double area=contourArea(contours[i]);
string resultc;
stringstream convertc;
convertc << area;
resultc=convertc.str();
char const* pchaa = resultc.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment contour area sorted=",pchaa);
}
//**END DEBUG UNSORTED CONTOUR AREA**//

__android_log_write(ANDROID_LOG_INFO, "augment contour area","prepare");

double perimeter;
vector<Point> approx1;
vector<Point> approx;
vector<vector<Point> >  screencnt;
Rect roi;
int pos=-1;

__android_log_write(ANDROID_LOG_INFO, "augment contour area","VM");
jfloat zeros[16]={0};
jfloatArray vM;
vM= (*env). NewFloatArray(16);
(*env). SetFloatArrayRegion(vM,0,16,zeros);
__android_log_write(ANDROID_LOG_INFO, "augment contour area","END VM");


int contLen=contours.size();
if(contLen>5){
contLen=5;
}
for(int i=0;i<contLen;i++){
//        vector<Point>  hull;
//        convexHull(contours[i],hull,false);
perimeter=arcLength(contours[i],true);
//** DEBUG PERIMETER **//
string resultc;
stringstream convertc;
convertc << perimeter;
resultc=convertc.str();
char const* pchaa = resultc.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment contour area perimeter=",pchaa);
//**END DEBUG PERIMETER**//

//** DEBUG approxPoly **//
approxPolyDP(contours[i],approx1,(0.01*perimeter),true);
string resulta;
stringstream converta;
converta << approx1.size();
resulta=converta.str();
char const* pchaap = resulta.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment contour area approx=",pchaap);
//**END DEBUG approxPoly**//
if(approx1.size()==4 && fabs(contourArea(approx1)) > 1000 && isContourConvex(approx1)){

int vtc = approx1.size();

// Get the cosines of all corners
double cos;
double maxCos=0;
for (int j = 2; j < vtc+1; j++){
cos=fabs(angle(approx1[j%vtc], approx1[j-2], approx1[j-1]));
maxCos=MAX(maxCos,cos);
}

string maxcostr;
stringstream maxcosstring;
maxcosstring << maxCos;
maxcostr=maxcosstring.str();
char const* pchamax = maxcostr.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment contour area maxcos=",pchamax);

if(vtc==4 && maxCos <0.3){
approx=approx1;
pos=i;
i=contours.size();

//** DEBUG approx **//
string resultam;
stringstream convertam;
convertam << approx;
resultam=convertam.str();
char const* pchaam = resultam.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment contour area approx=",pchaam);
//**END DEBUG approx**//
}

}
}
//CONVERT POINTS TO ARRAY

if(pos!=-1){
int last=approx.size();
    Rect boundRect=boundingRect(approx);
    rectangle(image,boundRect.tl(),boundRect.br(),Scalar(0,255,0,255),2,8,0);
//drawContours(image,contours,pos,Scalar(255,0,0,255),2);
double rectPoints[last][2];
for(int i=0;i<last;i++) {
rectPoints[i][0]=approx[i].x;
rectPoints[i][1]=approx[i].y;
}
//END OF CONVERSION

//** ORDERING POINTS **//
//sum axis 1
double sum[last];
double s;
for(int i=0;i<last;i++) {
for(int j=0;j<2;j++){
s=s+rectPoints[i][j];
}
sum[i]=s;
s=0;
}
vector<double> sumx (sum,sum+last);
sort(sumx.begin(),sumx.end(),myfunction2);
double tlval=sumx[0];
double brval=sumx[last-1];
int tlpos;
int brpos;
for(int i=0;i<last;i++) {
for(int j=0;j<2;j++){
s=s+rectPoints[i][j];
}
if(s==tlval){
tlpos=i;
}
if(s==brval){
brpos=i;
}
s=0;
}
Point tl=approx[tlpos];
Point br=approx[brpos];

//diff axis=1
double diff[last];
int d=0;
for(int i=0;i<last;i++) {
for(int j=0;j<2;j++){
d=rectPoints[i][j]-d;
}
diff[i]=d;
d=0;
string resultc;
stringstream convertc;
convertc << diff[i];
resultc=convertc.str();
char const* pchaa = resultc.c_str();
__android_log_write(ANDROID_LOG_INFO, "augment app diff=",pchaa);
}
vector<double> diffx (diff,diff+last);
sort(diffx.begin(),diffx.end(),myfunction2);
double trval=diffx[0];
double blval=diffx[last-1];
int trpos=-1, blpos=-1;
int posused;
for(int i=0;i<last;i++) {
if(i!=tlpos && i!=brpos){
for(int j=0;j<2;j++){
d=rectPoints[i][j]-d;
}
if(d==trval){
trpos=i;
posused=i;
}
if(d==blval){
blpos=i;
posused=i;
}
d=0;
}
}
if(trpos==-1 || blpos==-1){
for(int i=0;i<last;i++){
if(i!=tlpos && i!=brpos && i!= posused){
if(trpos==-1){
trpos=i;
}else{
blpos=i;
}
}
}
}
Point tr=approx[trpos];
Point bl=approx[blpos];
//**END ORDERING POINTS**//
__android_log_write(ANDROID_LOG_INFO, "augment calibrate read intrinsic","input points ");

vector<Point2f> input;
input.push_back(tl);input.push_back(tr);input.push_back(br);input.push_back(bl);

__android_log_write(ANDROID_LOG_INFO, "augment calibrate read intrinsic","input points ok");

//**CALIBRATION & POSE**//
//        Mat camera_matrix=Mat(3,3,CV_64F); //intrinsic
//        Mat distortion_coeff=Mat(5,1,CV_64F);
//
//        char* path=(char*) nativeString;
//        char path_for_intrinsic[strlen(path)+100];
//        strcpy(path_for_intrinsic,path);
//
//        FileStorage fsIntrinsic;
//        fsIntrinsic.open(strcat(path_for_intrinsic,"/xml/sample_calibrate_intrinsic.xml"), FileStorage::READ);
//        if(!fsIntrinsic.isOpened()){
//            __android_log_write(ANDROID_LOG_INFO, "calibrate read intrinsic","File cant open!");
//        }else{
//            __android_log_write(ANDROID_LOG_INFO, "calibrate read intrinsic","File  openED!");
//            fsIntrinsic["intrinsic"] >> camera_matrix;
//        }
//        fsIntrinsic.release();
//        char path_for_distortion[strlen(path)+100];
//        strcpy(path_for_distortion,path);
//
//        FileStorage fsDistortion;
//        fsDistortion.open(strcat(path_for_distortion,"/xml/sample_calibrate_distortion.xml"), FileStorage::READ);
//        __android_log_write(ANDROID_LOG_INFO, "Can calibrate read distortion",path_for_distortion);
//        if(!fsDistortion.isOpened()){
//            __android_log_write(ANDROID_LOG_INFO, "calibrate read distortion","File cant open!");
//        }else{
//            __android_log_write(ANDROID_LOG_INFO, "calibrate read distortion","File openED!");
//            fsDistortion["distortion_coefficients"] >> distortion_coeff;
//        }
//        fsDistortion.release();


//**CAMERA MATRIX & DISTORTION MATRIX DEFAULT VALUES**
//**WITHOUT DIRECT CALIBRATION **

double focal_length=image.cols;
Point2d center=Point2d(image.cols/2,image.rows/2);
double fa=2*1.333;
cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << focal_length, 0, center.x, 0 , focal_length, center.y, 0, 0, 1);
cv::Mat distortion_coeff = cv::Mat::zeros(4,1,cv::DataType<double>::type); // Assuming no lens distortion

//** END OF CODE**//

//** CORNERSUBPIX **//
vector<Point3f> objpts;
objpts.push_back(Point3f(0,0,0));objpts.push_back(Point3f(1,0,0));
objpts.push_back(Point3f(1,1,0));objpts.push_back(Point3f(0,1,0));

Mat rvecs,tvecs;
cornerSubPix( img_gray, input, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.01 ));
__android_log_write(ANDROID_LOG_INFO, "augment Can calibrate count","cornerSubPix success!");

solvePnP(objpts,input,camera_matrix,distortion_coeff,rvecs,tvecs,false,1);
if(rvecs.rows > 0 && tvecs.cols > 0){
Mat rotation_mat=Mat(3,3,CV_64F);;
Rodrigues(rvecs,rotation_mat);
__android_log_write(ANDROID_LOG_INFO, "augment Can calibrate count","rodrigues success");

Mat viewMat  = Mat(4,4,CV_64F);
for(int i = 0; i < viewMat.rows; i++) {
for (int j = 0; j < viewMat.cols; j++) {
if(i==3){
if(j<3){
viewMat.at<double> (i,j) =0.0f;
}else{
viewMat.at<double> (i,j) =1.0f;
}
}else{
if(j<3){
viewMat.at<double> (i,j) = rotation_mat.at<double> (i,j);
}else{
viewMat.at<double> (i,j) = tvecs.at<double> (i,0);
}
}
}
}

__android_log_write(ANDROID_LOG_INFO, "Can calibrate count","viewMat ok");
cv::Mat cvToGl = cv::Mat::zeros(4, 4, CV_64F);
cvToGl.at<double>(0, 0) = 1.0f;
cvToGl.at<double>(1, 1) = -1.0f;// Invert the y axis
cvToGl.at<double>(2, 2) = -1.0f; // invert the z axis
cvToGl.at<double>(3, 3) = 1.0f;
viewMat = cvToGl * viewMat;

Mat transposeViewMat=Mat(4,4,CV_64F);
for(int i = 0; i < viewMat.rows; i++) {
for (int j = 0; j < viewMat.cols; j++) {
transposeViewMat.at<double> (j,i) = viewMat.at<double> (i,j);
}
}

jfloat newViewMat[16];
int x=0;
for(int i = 0; i < viewMat.rows; i++) {
for (int j = 0; j < viewMat.cols; j++) {
newViewMat[x]= transposeViewMat.at<double> (i,j);
x++;
}
}
vM = (*env). NewFloatArray(16);
(*env). SetFloatArrayRegion(vM,0,16,newViewMat);
//            (*env).DeleteLocalRef(newViewMat);

__android_log_write(ANDROID_LOG_INFO, "Can calibrate count","vM ok");

return vM;
}
//**END USING POSE**//
}
return vM;
}


bool compareContourAreas2 ( std::vector<cv::Point> contour1, std::vector<cv::Point> contour2 ) {
    double i = fabs( contourArea(cv::Mat(contour1)) );
    double j = fabs( contourArea(cv::Mat(contour2)) );
    return ( i > j );
}
bool myfunction2 (int i,int j) {
    return (i<j);
}
static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}
